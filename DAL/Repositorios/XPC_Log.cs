﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public class XPC_Log : Repositorio<Model.XPC_Log>, Repositorios.IXPC_Log
    {
        private xpComEntities Context;

        public XPC_Log()
        {
            Context = new xpComEntities();
        }

        ~XPC_Log()
        {
            Context.Dispose();
        }
    }
}
