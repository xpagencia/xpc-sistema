﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public class VTEX_SkuImage : Repositorio<Model.VTEX_SkuImage>, Repositorios.IVTEX_SkuImage
    {
        private xpComEntities Context;

        public VTEX_SkuImage()
        {
            Context = new xpComEntities();
        }

        ~VTEX_SkuImage()
        {
            Context.Dispose();
        }
    }
}
