﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.Repositorios
{
    public interface IRepositorio<T> where T : class
    {
        IQueryable<T> Get(Expression<Func<T, bool>> predicate);
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate, Func<T, object> orderSelector);
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate, Func<T, object> orderSelector, int itemInicial, int qtdItens);
        List<T> List(Expression<Func<T, bool>> predicate);
        T First(Expression<Func<T, bool>> predicate);
        T Find(params object[] key);
        IEnumerable<T> GetList();
        IEnumerable<T> GetList(Expression<Func<T, bool>> predicate);
        IQueryable<T> GetTodos();
        IEnumerable<T> GetAllOrderBy(Func<T, object> keySelector);
        IEnumerable<T> GetAllOrderByDescending(Func<T, object> keySelector);
        T Add(T entity);
        void Update(T entity);
        void Excluir(Func<T, bool> predicate);
        void Delete(T entity);
        void Commit();
        void Dispose();
    }
}
