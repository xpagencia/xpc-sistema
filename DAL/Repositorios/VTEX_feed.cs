﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public class VTEX_feed : Repositorio<Model.VTEX_feed>, Repositorios.IVTEX_Feed
    {
        private xpComEntities Context;

        public VTEX_feed()
        {
            Context = new xpComEntities();
        }

        ~VTEX_feed()
        {
            Context.Dispose();
        }
    }
}
