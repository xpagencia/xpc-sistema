﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.Repositorios
{
    public class Repositorio<T> : Repositorios.IRepositorio<T>, IDisposable where T : class
    {
        private xpComEntities Context;

        protected Repositorio()
        {
            Context = new xpComEntities();
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> predicate)
        {
            return Context.Set<T>().Where(predicate);
        }

        public IEnumerable<T> Get(Expression<Func<T, bool>> predicate, Func<T, object> orderSelector)
        {
            return Context.Set<T>().Where(predicate).OrderBy(orderSelector);
        }

        public List<T> List(Expression<Func<T, bool>> predicate)
        {
            return Context.Set<T>().Where(predicate).ToList();
        }

        public IEnumerable<T> Get(Expression<Func<T, bool>> predicate, Func<T, object> orderSelector, int itemInicial, int qtdItens)
        {
            IEnumerable<T> sqlRetorno;
            if (itemInicial == 1)
                sqlRetorno = Context.Set<T>().Where(predicate).OrderBy(orderSelector).Take(qtdItens);
            else
                sqlRetorno = Context.Set<T>().Where(predicate).OrderBy(orderSelector).Skip(itemInicial - 1).Take(qtdItens);
            return sqlRetorno;
        }

        public IQueryable<T> GetTodos()
        {
            return Context.Set<T>();
        }
        public T First(Expression<Func<T, bool>> predicate)
        {
            return Context.Set<T>().Where(predicate).FirstOrDefault();
        }
        public T Find(params object[] key)
        {
            return Context.Set<T>().Find(key);
        }
        public IEnumerable<T> GetList()
        {
            return Context.Set<T>().ToList();
        }
        public IEnumerable<T> GetList(Expression<Func<T, bool>> predicate)
        {
            return Context.Set<T>().Where(predicate).ToList();
        }
        public IEnumerable<T> GetAllOrderBy(Func<T, object> keySelector)
        {
            return Context.Set<T>().OrderBy(keySelector).ToList();
        }
        public IEnumerable<T> GetAllOrderByDescending(Func<T, object> keySelector)
        {
            return Context.Set<T>().OrderByDescending(keySelector).ToList();
        }
        public void Commit()
        {
            Context.SaveChanges();
        }
        public T Add(T entity)
        {
            return Context.Set<T>().Add(entity);
        }
        public void Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }
        public void Excluir(Func<T, bool> predicate)
        {
            Context.Set<T>()
                .Where(predicate).ToList()
                .ForEach(del => Context.Set<T>().Remove(del));
        }
        public void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
        }
        public void Dispose()
        {
            if (Context != null)
            {
                Context.Dispose();
            }
            GC.SuppressFinalize(this);
        }
    }
}
