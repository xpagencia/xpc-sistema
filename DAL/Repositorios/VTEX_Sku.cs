﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public class VTEX_Sku : Repositorio<Model.VTEX_Sku>, Repositorios.IVTEX_Sku
    {
        private xpComEntities Context;

        public VTEX_Sku()
        {
            Context = new xpComEntities();
        }

        ~VTEX_Sku()
        {
            Context.Dispose();
        }
    }
}
