﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public interface ICron: IRepositorio<Model.Cron>
    {
        Model.Cron Iniciar(Model.Cron _cron);
        Model.Cron Finalizar(Model.Cron _cron);
    }
}
