﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public class VTEX_Marca : Repositorio<Model.VTEX_Marca>, Repositorios.IVTEX_Marca
    {
        private xpComEntities Context;

        public VTEX_Marca()
        {
            Context = new xpComEntities();
        }

        ~VTEX_Marca()
        {
            Context.Dispose();
        }
    }
}
