﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public interface IUsuario : IRepositorio<Model.Usuario>
    {
        List<Model.Usuario> GetList();
    }
}
