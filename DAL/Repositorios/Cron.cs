﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public class Cron: Repositorio<Model.Cron>, Repositorios.ICron
    {
        private xpComEntities Context;

        public Cron()
        {
            Context = new xpComEntities();
        }

        ~Cron()
        {
            Context.Dispose();
        }

        public Model.Cron Iniciar(Model.Cron _cron)
        {
            _cron.dtFinal = null;
            _cron.dtInicial = DateTime.Now;

            Model.Cron cron = Get(x => x.sistema == _cron.sistema && x.tarefa == _cron.tarefa && x.secao == _cron.secao && x.identificador == _cron.identificador).FirstOrDefault();
            if(cron == null)
            {
                cron = Add(_cron);
            }
            else
            {
                Update(_cron);
                cron = _cron;
            }
            Commit();
            return cron;
        }

        public Model.Cron Finalizar(Model.Cron _cron)
        {
            _cron.dtFinal = DateTime.Now;
            Update(_cron);
            Commit();
            return _cron;
        }
    }
}
