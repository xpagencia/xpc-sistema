﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public class VTEX_SkuEspecificacao : Repositorio<Model.VTEX_SkuEspecificacao>, Repositorios.IVTEX_SkuEspecificacao
    {
        private xpComEntities Context;

        public VTEX_SkuEspecificacao()
        {
            Context = new xpComEntities();
        }

        ~VTEX_SkuEspecificacao()
        {
            Context.Dispose();
        }
    }
}
