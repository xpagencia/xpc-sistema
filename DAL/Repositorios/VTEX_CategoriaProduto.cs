﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public class VTEX_CategoriaProduto : Repositorio<Model.VTEX_CategoriaProduto>, Repositorios.IVTEX_CategoriaProduto
    {
        private xpComEntities Context;

        public VTEX_CategoriaProduto()
        {
            Context = new xpComEntities();
        }

        ~VTEX_CategoriaProduto()
        {
            Context.Dispose();
        }
    }
}
