﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public class VTEX_Categoria : Repositorio<Model.VTEX_Categoria>, Repositorios.IVTEX_Categoria
    {
        private xpComEntities Context;

        public VTEX_Categoria()
        {
            Context = new xpComEntities();
        }

        ~VTEX_Categoria()
        {
            Context.Dispose();
        }
    }
}
