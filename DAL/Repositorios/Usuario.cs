﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public class Usuario : Repositorio<Model.Usuario>, Repositorios.IUsuario
    {
        private xpComEntities Context;

        public Usuario()
        {
            Context = new xpComEntities();
        }

        ~Usuario()
        {
            Context.Dispose();
        }

        /// <summary>
        /// Lista todos os usuários que fazem login no sistema
        /// </summary>
        /// <returns></returns>
        public List<Model.Usuario> GetList()
        {
            return Context.Usuarios.ToList();
        }
    }
}
