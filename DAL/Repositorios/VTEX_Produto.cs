﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public class VTEX_Produto : Repositorio<Model.VTEX_Produto>, Repositorios.IVTEX_Produto
    {
        private xpComEntities Context;

        public VTEX_Produto()
        {
            Context = new xpComEntities();
        }

        ~VTEX_Produto()
        {
            Context.Dispose();
        }
    }
}
