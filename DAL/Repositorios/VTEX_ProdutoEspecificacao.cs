﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public class VTEX_ProdutoEspecificacao : Repositorio<Model.VTEX_ProdutoEspecificacao>, Repositorios.IVTEX_ProdutoEspecificacao
    {
        private xpComEntities Context;

        public VTEX_ProdutoEspecificacao()
        {
            Context = new xpComEntities();
        }

        ~VTEX_ProdutoEspecificacao()
        {
            Context.Dispose();
        }
    }
}
