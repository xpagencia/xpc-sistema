﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositorios
{
    public class VTEX_Pedido : Repositorio<Model.VTEX_Pedido>, Repositorios.IVTEX_Pedido
    {
        private xpComEntities Context;

        public VTEX_Pedido()
        {
            Context = new xpComEntities();
        }

        ~VTEX_Pedido()
        {
            Context.Dispose();
        }
    }
}
