﻿using System.Linq;
using Arcnet.MyConvert;


namespace BLL.Usuario
{
    public class Usuario
    {
        DAL.Repositorios.IUsuario _usuario;

        public Usuario()
        {
            _usuario = new DAL.Repositorios.Usuario();
        }

        public Model.Usuario Login(string usuario, string senha)
        {
            string senhaCrypt = MyConvert.ToMd5(senha);
            return _usuario.Get(p => p.usuario1 == usuario && p.senha == senhaCrypt).FirstOrDefault();
        }

        public Model.Usuario Get(int idUsuario)
        {
            return _usuario.Get(p => p.IdUsuario == idUsuario).FirstOrDefault();
        }
    }
}
