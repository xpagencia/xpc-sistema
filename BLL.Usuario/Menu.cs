﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Usuario.PermissaoAcesso
{
    public class Menu
    {
        DAL.Repositorios.IMenu _menu;

        public Menu()
        {
            _menu = new DAL.Repositorios.Menu();
        }

        public List<Model.Menu> GetArvore()
        {
            DAL.Repositorios.IMenu menuDAO = new DAL.Repositorios.Menu();
            IEnumerable<Model.Menu> query = menuDAO.GetList();
            List<Model.Menu> retorno = new List<Model.Menu>();
            /*
            retorno.AddRange(query.Where(x => !x.Todos && x.MenuPerfils.Where(y => y.IdPerfil == idPerfil).Count() > 0));
            retorno.AddRange(query.Where(x => x.Todos));
            */
            retorno.AddRange(query.Where(x => x.idMenuPai == null));
            return retorno;
        }
    }
}
