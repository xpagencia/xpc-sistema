﻿using System;
using System.Collections.Generic;

namespace BLL.VTEX.VTEX_ProdutoEspecificacao
{
    public class Dal
    {
        DAL.Repositorios.IVTEX_ProdutoEspecificacao _entidade;
        public Dal()
        {
            _entidade = new DAL.Repositorios.VTEX_ProdutoEspecificacao();
        }

        public List<Model.VTEX_ProdutoEspecificacao> Lista()
        {
            return (List<Model.VTEX_ProdutoEspecificacao>)_entidade.GetList();
        }

        public List<Model.VTEX_ProdutoEspecificacao> Lista(int idProduto)
        {
            return (List<Model.VTEX_ProdutoEspecificacao>)_entidade.GetList(x=> x.idVTEXProduto == idProduto);
        }

        public Model.VTEX_ProdutoEspecificacao Get(int id)
        {
            return _entidade.First(x => x.IdVTEXProdutoEspecification == id);
        }

        public Model.VTEX_ProdutoEspecificacao Get(int idProduto, string campoNome)
        {
            return _entidade.First(x => x.idVTEXProduto == idProduto && x.campoNome == campoNome);
        }
    }
}
