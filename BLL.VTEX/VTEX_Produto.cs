﻿using System;
using System.Collections.Generic;

namespace BLL.VTEX.VTEX_Produto
{
    public class Dal
    {
        DAL.Repositorios.IVTEX_Produto _entidade;
        public Dal()
        {
            _entidade = new DAL.Repositorios.VTEX_Produto();
        }

        public List<Model.VTEX_Produto> Lista()
        {
            return (List<Model.VTEX_Produto>)_entidade.GetList();
        }

        public List<Model.VTEX_Produto> ListaStatus(int status)
        {
            return (List<Model.VTEX_Produto>)_entidade.List(x => x.fl_sinalizacao == status);
        }

        public Model.VTEX_Produto Get(int id)
        {
            return _entidade.First(x => x.IdVTEXProduto == id);
        }

        public bool UpdateSinalizacao(Model.VTEX_Produto _item)
        {
            Model.VTEX_Produto item = _entidade.First(x => x.IdVTEXProduto == _item.IdVTEXProduto);
            if (item == null)
            {
                return false;
            }
            item.fl_sinalizacao = _item.fl_sinalizacao;
            _entidade.Commit();
            return true;
        }

        public Model.VTEX_CategoriaProduto GetCategoriaPrincipal(int idVTEXProduto)
        {
            Model.VTEX_Produto produtos = _entidade.First(x=> x.IdVTEXProduto == idVTEXProduto);
            foreach (Model.VTEX_CategoriaProduto item in produtos.VTEX_CategoriaProduto)
            {
                if(item.fl_principal == true)
                {
                    return item;
                }
            }
            return null;
        }

        public List<Model.VTEX_CategoriaProduto> ListaCategoriasSimilaresPendentes(int idVTEXProduto)
        {
            List<Model.VTEX_CategoriaProduto> lista = new List<Model.VTEX_CategoriaProduto>();
            Model.VTEX_Produto produtos = _entidade.First(x => x.IdVTEXProduto == idVTEXProduto);
            foreach (Model.VTEX_CategoriaProduto item in produtos.VTEX_CategoriaProduto)
            {
                if (item.fl_principal != true && item.fl_sinalizacao == 0)
                {
                    lista.Add(item);
                }
            }
            return lista; 
        }
    }
}
