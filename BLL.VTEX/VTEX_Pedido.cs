﻿using System;
using System.Collections.Generic;

namespace BLL.VTEX.Pedido
{
    public class Dal
    {
        DAL.Repositorios.IVTEX_Pedido _entidade;
        public Dal()
        {
            _entidade = new DAL.Repositorios.VTEX_Pedido();
        }

        public List<Model.VTEX_Pedido> Lista()
        {
            return (List<Model.VTEX_Pedido>)_entidade.GetList();
        }

        public Model.VTEX_Pedido Item(int id)
        {
            return (Model.VTEX_Pedido)_entidade.First(x => x.IdVTEXPedido == id);
        }

        public Model.VTEX_Pedido ItemByOrderId(string orderId)
        {
            return (Model.VTEX_Pedido)_entidade.First(x => x.orderId == orderId);
        }

        public bool InsertUpdate(Model.VTEX_Pedido _item)
        {
            foreach (Model.VTEX_PedidoCliente cliente in _item.VTEX_PedidoCliente)
            {
                if (cliente.cep.Length > 8) cliente.cep = cliente.cep.Substring(0, 8);
            }
            _item.fl_sinalizacao = 0;

            Model.VTEX_Pedido item = _entidade.First(x => x.orderId == _item.orderId);
            if (item == null)
            {
                _entidade.Add(_item);
            }
            _entidade.Commit();
            return true;
        }

        public bool UpdateSinalizacao(Model.VTEX_Pedido _pedido)
        {
            Model.VTEX_Pedido item = Get(_pedido.IdVTEXPedido);
            if (item == null) return false;
            item.fl_sinalizacao = _pedido.fl_sinalizacao;
            _entidade.Commit();
            return true;
        }

        public List<Model.VTEX_Pedido> Lista(string status)
        {
            return (List<Model.VTEX_Pedido>)_entidade.GetList(x => x.status == status);
        }

        public List<Model.VTEX_Pedido> ListaSinalizacao(int fl_sinalizacao)
        {
            return (List<Model.VTEX_Pedido>)_entidade.GetList(x => x.fl_sinalizacao == fl_sinalizacao);
        }

        public Model.VTEX_Pedido Get(int id)
        {
            return _entidade.First(x => x.IdVTEXPedido == id);
        }

        public Model.VTEX_Pedido Get(string orderId)
        {
            return _entidade.First(x => x.orderId == orderId);
        }
    }
}
