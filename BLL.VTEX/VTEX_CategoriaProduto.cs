﻿using System;
using System.Collections.Generic;

namespace BLL.VTEX.VTEX_CategoriaProduto
{
    public class Dal
    {
        DAL.Repositorios.IVTEX_CategoriaProduto _entidade;
        public Dal()
        {
            _entidade = new DAL.Repositorios.VTEX_CategoriaProduto(); 
        }

        public List<Model.VTEX_CategoriaProduto> Lista()
        {
            return (List<Model.VTEX_CategoriaProduto>)_entidade.GetList();
        }

        public List<Model.VTEX_CategoriaProduto> ListaStatus(int status)
        {
            return (List<Model.VTEX_CategoriaProduto>)_entidade.List(x => x.fl_sinalizacao == status);
        }

        public Model.VTEX_CategoriaProduto Get(int id)
        {
            return _entidade.First(x => x.IdVTEXCategoriaProduto == id);
        }

        public bool UpdateSinalizacao(Model.VTEX_CategoriaProduto _item)
        {
            Model.VTEX_CategoriaProduto item = _entidade.First(x => x.IdVTEXCategoriaProduto == _item.IdVTEXCategoriaProduto);
            if (item == null)
            {
                return false;
            }
            item.fl_sinalizacao = _item.fl_sinalizacao;
            _entidade.Commit();
            return true;
        }



    }
}
