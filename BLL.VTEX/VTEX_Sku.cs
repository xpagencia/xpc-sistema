﻿using System;
using System.Collections.Generic;

namespace BLL.VTEX.VTEX_Sku
{
    public class Dal
    {
        DAL.Repositorios.IVTEX_Sku _entidade;
        public Dal()
        {
            _entidade = new DAL.Repositorios.VTEX_Sku();
        }

        public List<Model.VTEX_Sku> Lista()
        {
            return (List<Model.VTEX_Sku>)_entidade.GetList();
        }

        public List<Model.VTEX_Sku> ListaStatus(int status)
        {
            return (List<Model.VTEX_Sku>)_entidade.List(x => x.fl_sinalizacao == status);
        }
        public List<Model.VTEX_Sku> ListaStatus(int status, int idVTEXProduto)
        {
            return (List<Model.VTEX_Sku>)_entidade.List(x => x.IdVTEXProduto == idVTEXProduto && x.fl_sinalizacao == status);
        }

        public Model.VTEX_Sku Get(int id)
        {
            return _entidade.First(x => x.IdVTEXSku == id);
        }

        public bool UpdateSinalizacao(Model.VTEX_Sku _item)
        {
            Model.VTEX_Sku item = _entidade.First(x => x.IdVTEXSku == _item.IdVTEXSku);
            if (item == null)
            {
                return false;
            }
            item.fl_sinalizacao = _item.fl_sinalizacao;
            _entidade.Commit();
            return true;
        }
    }
}
