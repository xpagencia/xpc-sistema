﻿using System;
using System.Collections.Generic;

namespace BLL.VTEX.VTEX_SkuEspecificacao
{
    public class Dal
    {
        DAL.Repositorios.IVTEX_SkuEspecificacao _entidade;
        public Dal()
        {
            _entidade = new DAL.Repositorios.VTEX_SkuEspecificacao();
        }

        public List<Model.VTEX_SkuEspecificacao> Lista()
        {
            return (List<Model.VTEX_SkuEspecificacao>)_entidade.GetList();
        }

        public List<Model.VTEX_SkuEspecificacao> Lista(int idSku)
        {
            return (List<Model.VTEX_SkuEspecificacao>)_entidade.GetList(x=> x.idVTEXSku == idSku);
        }

        public Model.VTEX_SkuEspecificacao Get(int id)
        {
            return _entidade.First(x => x.IdVTEXSKUEspecification == id);
        }

        public Model.VTEX_SkuEspecificacao Get(int idSku, string campoNome)
        {
            return _entidade.First(x => x.idVTEXSku == idSku && x.campoNome == campoNome);
        }
    }
}
