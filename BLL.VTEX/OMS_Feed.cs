﻿using System;
using System.Collections.Generic;

namespace BLL.VTEX.OMS_Feed
{
    public class Dal
    {
        DAL.Repositorios.IVTEX_Feed _entidade;
        public Dal()
        {
            _entidade = new DAL.Repositorios.VTEX_feed();
        }

        public List<Model.VTEX_feed> Lista()
        {
            return (List<Model.VTEX_feed>)_entidade.GetList();
        }

        public Model.VTEX_feed Item(int id)
        {
            return (Model.VTEX_feed)_entidade.First(x => x.id == id);
        }

        public Model.VTEX_feed ItemByOrderId(string orderId)
        {
            return (Model.VTEX_feed)_entidade.First(x => x.orderId == orderId);
        }

        public bool IsLastItem(Model.VTEX_feed _item)
        {
            Model.VTEX_feed item = _entidade.First(x => x.orderId == _item.orderId && x.dateTime > _item.dateTime && x.status == _item.status);
            return item != null && item.id > 0;
        }

        public bool Exists(Model.VTEX_feed _item)
        {
            Model.VTEX_feed item = _entidade.First(x => x.orderId == _item.orderId && x.dateTime == _item.dateTime && x.status == _item.status);
            return item != null && item.id > 0;
        }

        public bool InsertUpdate(Model.VTEX_feed _item)
        {
            try
            {
                if (!Exists(_item))
                {
                    _entidade.Add(_item);
                    _entidade.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
