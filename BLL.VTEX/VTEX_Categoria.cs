﻿using System;
using System.Collections.Generic;

namespace BLL.VTEX.VTEX_Categoria
{
    public class Dal
    {
        DAL.Repositorios.IVTEX_Categoria _entidade;
        public Dal()
        {
            _entidade = new DAL.Repositorios.VTEX_Categoria();
        }

        public List<Model.VTEX_Categoria> Lista()
        {
            return (List<Model.VTEX_Categoria>)_entidade.GetList();
        }

        public List<Model.VTEX_Categoria> ListaStatus(int status)
        {
            return (List<Model.VTEX_Categoria>)_entidade.List(x => x.fl_sinalizacao == status);
        }

        public Model.VTEX_Categoria Get(int id)
        {
            return _entidade.First(x => x.IdVTEXCategoria == id);
        }

        public bool UpdateSinalizacao(Model.VTEX_Categoria _item)
        {
            Model.VTEX_Categoria item = _entidade.First(x => x.IdVTEXCategoria == _item.IdVTEXCategoria);
            if (item == null)
            {
                return false;
            }
            item.fl_sinalizacao = _item.fl_sinalizacao;
            _entidade.Commit();
            return true;
        }

        public Model.VTEX_Categoria GetDepartamento(int idVTEXCategoria)
        {
            Model.VTEX_Categoria item = _entidade.First(x => x.IdVTEXCategoria == idVTEXCategoria);
            if (item.IdCategoriaPai == 0 || item.IdCategoriaPai == null)
                return item;
            else
                return GetDepartamento((int)item.IdCategoriaPai);            
        }
    }
}
