﻿using System;
using System.Collections.Generic;

namespace BLL.XPC.XPC_Log
{
    public class Dal
    {
        DAL.Repositorios.IXPC_Log _entidade;
        public Dal()
        {
            _entidade = new DAL.Repositorios.XPC_Log();
        }

        public List<Model.XPC_Log> Lista()
        {
            return (List<Model.XPC_Log>)_entidade.GetList();
        }

        public Model.XPC_Log Get(int id)
        {
            return _entidade.First(x => x.idXPCLog == id);
        }

        public Model.XPC_Log Incluir(Model.XPC_Log log)
        {
            log.data = DateTime.Now;
            log = _entidade.Add(log);
            _entidade.Commit();
            return log;
        }
        
        public static void Incluir(int idChave, string tabela, string tipo, string mensagem)
        {
            Dal clog = new Dal();
            Model.XPC_Log log = new Model.XPC_Log();
            log.idChave = idChave;
            log.tabela = tabela;
            log.tipoLog = tipo;
            log.mensagem = mensagem;
            log = clog.Incluir(log);
        }
        public static void Incluir(string tipo, string mensagem)
        {
            Dal clog = new Dal();
            Model.XPC_Log log = new Model.XPC_Log();
            log.tipoLog = tipo;
            log.mensagem = mensagem;
            log = clog.Incluir(log);
        }
    }
}
