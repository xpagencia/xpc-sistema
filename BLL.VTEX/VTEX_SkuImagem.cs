﻿using System;
using System.Collections.Generic;

namespace BLL.VTEX.VTEX_SkuImagem
{
    public class Dal
    {
        DAL.Repositorios.IVTEX_SkuImage _entidade;
        public Dal()
        {
            _entidade = new DAL.Repositorios.VTEX_SkuImage();
        }

        public List<Model.VTEX_SkuImage> Lista()
        {
            return (List<Model.VTEX_SkuImage>)_entidade.GetList();
        }

        public List<Model.VTEX_SkuImage> ListaStatus(int status)
        {
            return (List<Model.VTEX_SkuImage>)_entidade.List(x => x.fl_sinalizacao == status);
        }
        public List<Model.VTEX_SkuImage> ListaStatus(int status, int idSkuImage)
        {
            return (List<Model.VTEX_SkuImage>)_entidade.List(x => x.IdSkuImage == idSkuImage && x.fl_sinalizacao == status);
        }

        public Model.VTEX_SkuImage Get(int id)
        {
            return _entidade.First(x => x.IdSkuImage == id);
        }

        public bool UpdateSinalizacao(Model.VTEX_SkuImage _item)
        {
            Model.VTEX_SkuImage item = _entidade.First(x => x.IdSku == _item.IdSku);
            if (item == null)
            {
                return false;
            }
            item.fl_sinalizacao = _item.fl_sinalizacao;
            _entidade.Commit();
            return true;
        }
    }
}
