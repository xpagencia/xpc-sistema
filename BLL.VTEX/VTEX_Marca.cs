﻿using System;
using System.Collections.Generic;

namespace BLL.VTEX.VTEX_Marca
{
    public class Dal
    {
        DAL.Repositorios.IVTEX_Marca _entidade;
        public Dal()
        {
            _entidade = new DAL.Repositorios.VTEX_Marca();
        }

        public List<Model.VTEX_Marca> Lista()
        {
            return (List<Model.VTEX_Marca>)_entidade.GetList();
        }

        public List<Model.VTEX_Marca> ListaStatus(int status)
        {
            return (List<Model.VTEX_Marca>)_entidade.List(x=> x.fl_sinalizacao == status);
        }

        public Model.VTEX_Marca Get(int id)
        {
            return _entidade.First(x => x.IdVTEXMarca == id);
        }

        public bool UpdateSinalizacao(Model.VTEX_Marca _item)
        {
            Model.VTEX_Marca item = _entidade.First(x => x.IdVTEXMarca == _item.IdVTEXMarca);
            if(item == null)
            {
                return false;
            }
            item.fl_sinalizacao = _item.fl_sinalizacao;
            _entidade.Commit();
            return true;
        }
    }
}
