﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Sistema
{
    public class Cron
    {
        DAL.Repositorios.ICron _entidade;
        Model.Cron _cron;
        private APIGlobal.IUtil util = new APIGlobal.Util();

        private void Init(Sistema sistema, Tarefa tarefa, Secao secao, string identificador)
        {
            _entidade = new DAL.Repositorios.Cron();
            _cron = Get(sistema, tarefa, secao, identificador);
            if (_cron == null)
            {
                _cron = new Model.Cron();
                _cron.sistema = util.GetEnumDescription(sistema);
                _cron.tarefa = util.GetEnumDescription(tarefa);
                _cron.secao = util.GetEnumDescription(secao);
                _cron.identificador = identificador;
                _cron.ativo = true;
            }
        }

        public Cron(Sistema sistema, Tarefa tarefa, Secao secao)
        {
            Init(sistema, tarefa, secao, "");
        }

        public Cron(Sistema sistema, Tarefa tarefa, Secao secao, string identificador)
        {
            Init(sistema, tarefa, secao, identificador);
        }

        public enum Sistema
        {
            [System.ComponentModel.Description("Integração Ideológica-VTEX")]
            Integracao_Ideologica_Vtex
        };

        public enum Tarefa
        {
            [System.ComponentModel.Description("lojas")]
            lojas,
            [System.ComponentModel.Description("categorias")]
            categorias,
            [System.ComponentModel.Description("produtos")]
            produtos,
            [System.ComponentModel.Description("lojas-status")]
            lojas_status,
            [System.ComponentModel.Description("clientes")]
            clientes,
            [System.ComponentModel.Description("pedidos")]
            pedidos
        };

        public enum Secao
        {
            [System.ComponentModel.Description("Ler ERP")]
            LerErp,
            [System.ComponentModel.Description("Salva VTEX")]
            SalvaVtex,
            [System.ComponentModel.Description("Null")]
            Null,
            [System.ComponentModel.Description("Ler VTEX")]
            LerVTex,
            [System.ComponentModel.Description("Ler XP")]
            LerXP
        };

        public void Iniciar()
        {
            if (!EmAndamento())
                _cron = _entidade.Iniciar(_cron);
            else
                throw (new Exception("Não é possível iniciar essa tarefa pois ela já está em andamento por outro processo."));
        }

        public void Finalizar()
        {
            if(EmAndamento())
                _cron = _entidade.Finalizar(_cron);
        }

        private Model.Cron Get(Sistema sistema, Tarefa tarefa, Secao secao, string identificador)
        {
            string _sistema = util.GetEnumDescription(sistema);
            string _tarefa = util.GetEnumDescription(tarefa);
            string _secao = util.GetEnumDescription(secao);

            return _entidade.Get(x => x.sistema == _sistema 
                && x.tarefa == _tarefa
                && x.secao == _secao
                && x.identificador == identificador)
                .FirstOrDefault();
        }

        public bool EmAndamento()
        {
            return _cron.dtInicial != null && _cron.dtFinal == null;
        }
    }
}
