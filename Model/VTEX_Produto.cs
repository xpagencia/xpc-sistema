//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class VTEX_Produto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VTEX_Produto()
        {
            this.VTEX_CategoriaProduto = new HashSet<VTEX_CategoriaProduto>();
            this.VTEX_ProdutoEspecificacao = new HashSet<VTEX_ProdutoEspecificacao>();
            this.VTEX_Sku = new HashSet<VTEX_Sku>();
        }
    
        public int IdVTEXProduto { get; set; }
        public int IdVTEXMarca { get; set; }
        public string produto { get; set; }
        public string keywords { get; set; }
        public string url { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string descricaoCurta { get; set; }
        public string metaDescription { get; set; }
        public System.DateTime dataLancamento { get; set; }
        public string codigoReferencia { get; set; }
        public bool fl_ativo { get; set; }
        public short fl_sinalizacao { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VTEX_CategoriaProduto> VTEX_CategoriaProduto { get; set; }
        public virtual VTEX_Marca VTEX_Marca { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VTEX_ProdutoEspecificacao> VTEX_ProdutoEspecificacao { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VTEX_Sku> VTEX_Sku { get; set; }
    }
}
