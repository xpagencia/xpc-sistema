//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class VTEX_PedidoSku
    {
        public int IdVTEXPedidoSku { get; set; }
        public int idVTEXPedido { get; set; }
        public int IdVTEXSku { get; set; }
        public decimal precoUnitario { get; set; }
        public decimal pesoUnitario { get; set; }
        public short quantity { get; set; }
        public string idEstoque { get; set; }
        public string idRef { get; set; }
    
        public virtual VTEX_Pedido VTEX_Pedido { get; set; }
        public virtual VTEX_Sku VTEX_Sku { get; set; }
    }
}
