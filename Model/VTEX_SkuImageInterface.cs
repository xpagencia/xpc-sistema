﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public partial class VTEX_SkuImage
    {
        public string name
        {
            get
            {
                string nome = urlOriginal.Substring(urlOriginal.LastIndexOf("/") + 1);
                nome = nome.Substring(0,nome.LastIndexOf("."));
                return nome;
            }
        }
    }
}
