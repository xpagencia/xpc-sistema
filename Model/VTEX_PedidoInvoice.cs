//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class VTEX_PedidoInvoice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VTEX_PedidoInvoice()
        {
            this.VTEX_PedidoInvoiceItem = new HashSet<VTEX_PedidoInvoiceItem>();
        }
    
        public int IdVTEXPedidoInvoiceId { get; set; }
        public int IdVTEXPedido { get; set; }
        public System.DateTime date { get; set; }
        public int invoiceNumber { get; set; }
        public decimal invoiceValue { get; set; }
        public string invoiceKey { get; set; }
        public string invoiceUrl { get; set; }
        public string codigoTransportadora { get; set; }
        public string codigoRastreio { get; set; }
        public string urlRastreio { get; set; }
        public short fl_sinalizacao { get; set; }
    
        public virtual VTEX_Pedido VTEX_Pedido { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VTEX_PedidoInvoiceItem> VTEX_PedidoInvoiceItem { get; set; }
    }
}
