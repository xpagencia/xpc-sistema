﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIGlobal;
using Newtonsoft.Json.Linq;
using VTEX.Webservice.WSDL;

namespace XPBI.Integracao.ERP_VTEX.Vtex
{
    public class Categoria : Configuracao
    {
        private Integracao.VTex.Categorias vtex_categoria;
        private BLL.VTEX.VTEX_Categoria.Dal cCategoria = new BLL.VTEX.VTEX_Categoria.Dal();

        public Categoria(string loja, string appKey, string appToken, string userSoap, string passSoap)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            UserSoap = userSoap;
            PassSoap = passSoap;
            vtex_categoria = new Integracao.VTex.Categorias(Loja, AppKey, AppToken, UserSoap, PassSoap);
        }

        public Categoria(string loja)
        {
            Loja = loja;
        }

        /// <summary>
        /// Atualiza as informações de categorias dentro da VTEX
        /// </summary>
        /// <returns></returns>
        public bool Importar()
        {
            try
            {
                //baixo a lista do ERP que está pendente de atualização na vtex
                List<Model.VTEX_Categoria> categorias = cCategoria.ListaStatus(0).OrderBy(x=>x.IdCategoriaPai).ToList();
                foreach (Model.VTEX_Categoria item in categorias)
                {
                    Incluir(item);
                }

                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "Categorias: " + ex.Message;
                Erros.Gravar();
                BLL.XPC.XPC_Log.Dal.Incluir("Categorias-importar", ex.Message);
                return false;
            }
        }

        public bool Incluir(Model.VTEX_Categoria item)
        {
            try
            {
                if(item.IdCategoriaPai > 0)
                {
                    Model.VTEX_Categoria itemPai = cCategoria.Get(Convert.ToInt32(item.IdCategoriaPai));
                    if (itemPai != null && itemPai.fl_sinalizacao == 0)
                    {
                        Incluir(itemPai);
                    }
                }
                //incluo/altero o registro na vtex
                CategoryDTO categoriaDTO = new CategoryDTO();
                categoriaDTO.Id = item.IdVTEXCategoria;
                categoriaDTO.Description = item.descricao;
                categoriaDTO.IsActive = item.fl_ativo;
                categoriaDTO.Keywords = item.keywords;
                categoriaDTO.Name = item.categoria;
                categoriaDTO.Title = item.titulo;
                categoriaDTO.FatherCategoryId = (item.IdCategoriaPai > 0) ? item.IdCategoriaPai : null;
                categoriaDTO = vtex_categoria.InsertUpdate(categoriaDTO);
                if (categoriaDTO == null)
                    throw new Exception(vtex_categoria.Erros.Mensagem);
                else
                {
                    //atualiza o status no banco de que já foi atualizado
                    item.fl_sinalizacao = 1;
                    cCategoria.UpdateSinalizacao(item);
                }
                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "Categoria: ( " + item.IdVTEXCategoria.ToString() +  " ):" + ex.Message;
                Erros.Gravar();
                BLL.XPC.XPC_Log.Dal.Incluir("Categorias-incluir", ex.Message);

                item.fl_sinalizacao = 2;
                cCategoria.UpdateSinalizacao(item);
                return false;
            }
        }
    }
}
