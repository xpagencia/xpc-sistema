﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIGlobal;
using Newtonsoft.Json.Linq;
using VTEX.Webservice.WSDL;

namespace XPBI.Integracao.Ideologica_VTEX.Vtex
{
    public class Cliente : Configuracao
    {
        private Integracao.VTex.Clientes vtex_cliente;

        public Cliente(string loja, string ideologicaAppkey, string appKey, string appToken)
        {
            IdeologicaAppkey = ideologicaAppkey;
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            vtex_cliente = new Integracao.VTex.Clientes(Loja, AppKey, AppToken);
        }

        public Cliente(string loja, string ideologicaAppkey)
        {
            IdeologicaAppkey = ideologicaAppkey;
            Loja = loja;
        }

        /// <summary>
        /// Atualiza as informações de Clientes dentro da VTEX
        /// </summary>
        /// <returns></returns>
        public bool Importar()
        {
            bool retorno = false;
            BLL.Sistema.Cron cronERP = new BLL.Sistema.Cron(BLL.Sistema.Cron.Sistema.Integracao_Ideologica_Vtex, BLL.Sistema.Cron.Tarefa.clientes, BLL.Sistema.Cron.Secao.LerErp);
            BLL.Sistema.Cron cronVTEX = new BLL.Sistema.Cron(BLL.Sistema.Cron.Sistema.Integracao_Ideologica_Vtex, BLL.Sistema.Cron.Tarefa.clientes, BLL.Sistema.Cron.Secao.SalvaVtex);
            try
            {
                //baixo a lista da ideologica
                cronERP.Iniciar();
                Integracao.Ideologica.Metodos.Clientes ilogica_cliente = new Ideologica.Metodos.Clientes(IdeologicaAppkey);
                if (!ilogica_cliente.Importar())
                {
                    throw new Exception("Não foi possível importar os clientes da Ideológica para o sistema da XP.");
                }
                cronERP.Finalizar();

                cronVTEX.Iniciar();
                retorno = true;
                //ler banco  e comparo com a vtex. se não existir eu crio
                BLL.Ideologica.ILogica_Clientes.Dal dal = new BLL.Ideologica.ILogica_Clientes.Dal();
                List<Model.ILogica_clientes> xpIlogicaClientes = dal.Lista();
                foreach(Model.ILogica_clientes xpCliente in xpIlogicaClientes)
                {
                    //gravo o cliente na vtex
                    string nome = xpCliente.Nome;
                    string sobrenome = "";

                    if(xpCliente.Nome.IndexOf(" ") > -1)
                    {
                        sobrenome = nome.Substring(nome.IndexOf(" ")).Trim();
                        nome = nome.Substring(0, nome.IndexOf(" ")).Trim();                        
                    }

                    JObject body = new JObject();
                    body.Add("document", xpCliente.CPFCNPJ);
                    body.Add("firstName", nome);
                    body.Add("lastName", sobrenome);
                    body.Add("email", xpCliente.Email);
                    body.Add("homePhone", xpCliente.Telefone1);
                    body.Add("businessPhone", xpCliente.Telefone2);
                    body.Add("phone", xpCliente.Telefone3);
                    
                    if (!vtex_cliente.Criar(body))
                    {
                        retorno = false;
                    }
                }

                cronVTEX.Finalizar();
                return retorno;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                cronERP.Finalizar();
                cronVTEX.Finalizar();                
                return false;
            }
        }
    }
}
