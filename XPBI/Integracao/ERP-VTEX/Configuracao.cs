﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIGlobal;
using Newtonsoft.Json.Linq;
using VTEX.Webservice.WSDL;

namespace XPBI.Integracao.ERP_VTEX.Vtex
{
    public class Configuracao
    {
        public IErros Erros = new Erros();
        public string erp { get; set; }//verificar
        public string Loja { get; set; }
        public string AppKey { get; set; }
        public string AppToken { get; set; }
        public string UserSoap { get; set; }
        public string PassSoap { get; set; }
    }
}
   