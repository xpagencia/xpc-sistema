﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIGlobal;
using Newtonsoft.Json.Linq;
using VTEX.Webservice.WSDL;

namespace XPBI.Integracao.ERP_VTEX.Vtex
{
    public class Marca : Configuracao
    {
        private Integracao.VTex.Marcas vtex_marca;
        private BLL.VTEX.VTEX_Marca.Dal cMarca = new BLL.VTEX.VTEX_Marca.Dal();

        public Marca(string loja, string appKey, string appToken, string userSoap, string passSoap)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            UserSoap = userSoap;
            PassSoap = passSoap;
            vtex_marca = new Integracao.VTex.Marcas(Loja, AppKey, AppToken, UserSoap, PassSoap);
        }

        public Marca(string loja)
        {
            Loja = loja;
        }

        /// <summary>
        /// Atualiza as informações de categorias dentro da VTEX
        /// </summary>
        /// <returns></returns>
        public bool Importar()
        {
            try
            {
                //baixo a lista do ERP que está pendente de atualização na vtex
                List<Model.VTEX_Marca> marcas = cMarca.ListaStatus(0);
                foreach(Model.VTEX_Marca item in marcas)
                {
                    //incluo/altero o registro na vtex
                    BrandDTO marcaDTO = new BrandDTO();
                    marcaDTO.Id = item.IdVTEXMarca;
                    marcaDTO.Description = item.descricao;
                    marcaDTO.IsActive = item.fl_ativo;
                    marcaDTO.Keywords = item.keywords;
                    marcaDTO.Name = item.marca;
                    marcaDTO.Title = item.titulo;
                    marcaDTO = vtex_marca.InsertUpdate(marcaDTO);
                    if (marcaDTO == null)
                    {
                        Erros.Mensagem = "Marca (" + item.IdVTEXMarca.ToString() + "): " + vtex_marca.Erros.Mensagem;
                        Erros.Numero = vtex_marca.Erros.Numero;
                        Erros.Gravar();
                        BLL.XPC.XPC_Log.Dal.Incluir(item.IdVTEXMarca, "Marca", "Marca-importar", Erros.Mensagem);
                        Erros.Limpar();                        
                        item.fl_sinalizacao = 2;
                        cMarca.UpdateSinalizacao(item);                        
                    }
                        
                    else
                    {
                        //atualiza o status no banco de que já foi atualizado
                        item.fl_sinalizacao = 1;
                        cMarca.UpdateSinalizacao(item);
                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "Marcas: " + ex.Message;
                Erros.Gravar();
                BLL.XPC.XPC_Log.Dal.Incluir("Marca-importar", Erros.Mensagem);
                return false;
            }
        }
    }
}
