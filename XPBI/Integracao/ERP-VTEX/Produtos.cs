﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using VTEX.Webservice.WSDL;
using VTEX.Webservice;
using System.Configuration;

namespace XPBI.Integracao.ERP_VTEX.Vtex
{
    public class Produto : Configuracao
    {
        private Integracao.VTex.Produtos vtex_produto;
        private Integracao.VTex.Sku vtex_sku;
        private Integracao.VTex.Imagens vtex_imagens;
        private BLL.VTEX.VTEX_Produto.Dal cProduto = new BLL.VTEX.VTEX_Produto.Dal();
        private BLL.VTEX.VTEX_Sku.Dal cSku = new BLL.VTEX.VTEX_Sku.Dal();
        private BLL.VTEX.VTEX_SkuImagem.Dal cSkuImagem = new BLL.VTEX.VTEX_SkuImagem.Dal();
        private BLL.VTEX.VTEX_SkuEspecificacao.Dal cSkuEspecificacao = new BLL.VTEX.VTEX_SkuEspecificacao.Dal();
        private BLL.VTEX.VTEX_CategoriaProduto.Dal cCategoriaProduto = new BLL.VTEX.VTEX_CategoriaProduto.Dal();
        private BLL.VTEX.VTEX_ProdutoEspecificacao.Dal cProdutoEspecificacao = new BLL.VTEX.VTEX_ProdutoEspecificacao.Dal(); 

        public Produto(string loja, string appKey, string appToken, string userSoap, string passSoap)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            UserSoap = userSoap;
            PassSoap = passSoap;
            vtex_produto = new Integracao.VTex.Produtos(Loja, AppKey, AppToken, UserSoap, PassSoap);
            vtex_sku = new Integracao.VTex.Sku(Loja, AppKey, AppToken, UserSoap, PassSoap);
            vtex_imagens = new Integracao.VTex.Imagens(Loja, userSoap, passSoap);
        }

        /// <summary>
        /// Atualiza as informações de produtos do ERP dentro da VTEX
        /// </summary>
        /// <returns></returns>
        public bool Importar()
        {
            bool ocorreramErros = false;
            try
            {
                List<Model.VTEX_Sku> skus;
                BLL.VTEX.VTEX_Categoria.Dal cCategoria = new BLL.VTEX.VTEX_Categoria.Dal();
                //baixo a lista do ERP que está pendente de atualização na vtex
                List<Model.VTEX_Produto> produtos = cProduto.ListaStatus(0).OrderBy(x => x.IdVTEXProduto).ToList();
                foreach (Model.VTEX_Produto produto in produtos)
                {
                    //verifica se o produto ja existe na vtex. Se sim, atualiza e se não inclui.
                    ProductDTO objProduct1 = new ProductDTO();

                    //vtex_produto.Item(produto.IdVTEXProduto);                   }

                    Model.VTEX_CategoriaProduto categoriaProduto = cProduto.GetCategoriaPrincipal(produto.IdVTEXProduto);
                    if(categoriaProduto == null)
                    {
                        ocorreramErros = true;
                        Erros.Mensagem = "Produto (" + produto.IdVTEXProduto.ToString() + "): não possui categoria cadastrada.";
                        Erros.Numero = -1;
                        Erros.Gravar();
                        BLL.XPC.XPC_Log.Dal.Incluir(produto.IdVTEXProduto, "ProdutoCategoria", "Produto-importar", Erros.Mensagem);
                        continue;
                    }

                    string nomeProduto = (produto.produto.Length < 150) ? produto.produto : produto.produto.Substring(0, 150);
                    Int32 vtexIdDepartamento = cCategoria.GetDepartamento((int)categoriaProduto.IdVTEXCategoria).IdVTEXCategoria;
                    objProduct1.Id = produto.IdVTEXProduto;
                    objProduct1.BrandId = produto.IdVTEXMarca;
                    objProduct1.Description = produto.descricao;
                    objProduct1.DescriptionShort = produto.descricaoCurta;
                    objProduct1.Name = nomeProduto;
                    objProduct1.RefId = produto.codigoReferencia.ToString();
                    objProduct1.Title = produto.titulo;
                    objProduct1.CategoryId = categoriaProduto.IdVTEXCategoria;
                    objProduct1.DepartmentId = vtexIdDepartamento;
                    objProduct1.IsActive = produto.fl_ativo;
                    objProduct1.IsVisible = true;
                    objProduct1.KeyWords = produto.keywords;
                    objProduct1.MetaTagDescription = produto.metaDescription;
                    objProduct1.ShowWithoutStock = true;
                    objProduct1.LinkId = produto.url;
                    objProduct1 = vtex_produto.InsertUpdate(objProduct1);

                    if (objProduct1 == null)
                    {
                        Erros.Mensagem = "Produto incluir (" + nomeProduto + "): " + vtex_produto.Erros.Mensagem;
                        Erros.Numero = -1;
                        Erros.Gravar();
                        BLL.XPC.XPC_Log.Dal.Incluir(produto.IdVTEXProduto, "Produto", "Produto-importar", Erros.Mensagem);
                        ocorreramErros = true;
                        produto.fl_sinalizacao = 2;
                        cProduto.UpdateSinalizacao(produto);
                        continue;
                    }
                    produto.fl_sinalizacao = 1;
                    cProduto.UpdateSinalizacao(produto);
                    categoriaProduto.fl_sinalizacao = 1;
                    cCategoriaProduto.UpdateSinalizacao(categoriaProduto);

                    if (produto.VTEX_Sku.Count > 0)
                    {
                        foreach (Model.VTEX_Sku skutmp in produto.VTEX_Sku)
                        {
                            if (skutmp.VTEX_SkuKitPai.Count > 0)
                            {
                                //grava especificacao de produto informando que é um kit da integração.
                                if (!vtex_produto.ProductEspecificationInsert(produto.IdVTEXProduto, "isKit", (new string[] {"Sim"}) ))
                                {
                                    Erros.Mensagem = "Marcar Especificação produto(" + produto.IdVTEXProduto.ToString() + ") - kit: " + vtex_produto.Erros.Mensagem;
                                    Erros.Numero = vtex_produto.Erros.Numero;
                                    vtex_produto.Erros.Limpar();
                                    Erros.Gravar();
                                    BLL.XPC.XPC_Log.Dal.Incluir(produto.IdVTEXProduto, "ProdutoEspecificacao", "Produto-importar", Erros.Mensagem);
                                }
                                break;
                            }
                        }
                    }

                    //incluir especificações do produto
                    List<Model.VTEX_ProdutoEspecificacao> ProdutoEspecificacoes = cProdutoEspecificacao.Lista(produto.IdVTEXProduto);
                    foreach (Model.VTEX_ProdutoEspecificacao especificacao in ProdutoEspecificacoes)
                    {
                        if (!vtex_produto.ProductEspecificationInsert(produto.IdVTEXProduto, especificacao.campoNome, especificacao.campoValor.Split(',')))
                        {
                            Erros.Mensagem = "Marcar Especificação produto(" + produto.IdVTEXProduto.ToString() + ") - " + especificacao.campoNome + ": " + vtex_produto.Erros.Mensagem;
                            Erros.Numero = vtex_produto.Erros.Numero;
                            vtex_produto.Erros.Limpar();
                            Erros.Gravar();
                            BLL.XPC.XPC_Log.Dal.Incluir(produto.IdVTEXProduto, "ProdutoEspecificacao", "Produto-importar", Erros.Mensagem);
                        }
                    }

                    skus = cSku.ListaStatus(0, produto.IdVTEXProduto).OrderBy(x => x.IdVTEXSku).ToList();
                    foreach(Model.VTEX_Sku sku in skus)
                    {
                        IncluirSku(sku);
                    }

                    List<Model.VTEX_CategoriaProduto> categoriaProdutosPendentes = cProduto.ListaCategoriasSimilaresPendentes(produto.IdVTEXProduto);
                    foreach (Model.VTEX_CategoriaProduto item in categoriaProdutosPendentes)
                    {
                        vtex_produto.ProductSetSimilarCategory(item.IdVTEXProduto, item.IdVTEXCategoria);
                        if(vtex_produto.Erros.Mensagem != null && vtex_produto.Erros.Mensagem.Length > 0)
                        {
                            Erros.Mensagem = "Produto categoria similar (" + nomeProduto + "): " + vtex_produto.Erros.Mensagem;
                            Erros.Numero = -1;
                            Erros.Gravar();
                            BLL.XPC.XPC_Log.Dal.Incluir(produto.IdVTEXProduto, "ProdutoCategoriaSimilar", "Produto-importar-categoriasimilar", Erros.Mensagem);
                            ocorreramErros = true;
                            item.fl_sinalizacao = 2;
                            vtex_produto.Erros.Limpar();
                        }else
                        {
                            item.fl_sinalizacao = 1;
                        }
                        cCategoriaProduto.UpdateSinalizacao(item);
                    }
                }

                skus = cSku.ListaStatus(0).OrderBy(x=>x.IdVTEXSku).ToList();
                foreach (Model.VTEX_Sku sku in skus)
                {
                    IncluirSku(sku);
                }

                skus = cSku.ListaStatus(10).OrderBy(x => x.IdVTEXSku).ToList(); //estoque e preço
                foreach (Model.VTEX_Sku sku in skus)
                {
                    IncluirSku(sku);
                }

                skus = cSku.ListaStatus(11).OrderBy(x => x.IdVTEXSku).ToList(); // estoque
                foreach (Model.VTEX_Sku sku in skus)
                {
                    IncluirSku(sku);
                }

                skus = cSku.ListaStatus(12).OrderBy(x => x.IdVTEXSku).ToList(); // preço
                foreach (Model.VTEX_Sku sku in skus)
                {
                    IncluirSku(sku);
                }

                List<Model.VTEX_CategoriaProduto> categoriaProdutos = cCategoriaProduto.ListaStatus(0).Where(x => x.fl_principal == false).ToList();
                foreach (Model.VTEX_CategoriaProduto item in categoriaProdutos)
                {
                    vtex_produto.ProductSetSimilarCategory(item.IdVTEXProduto, item.IdVTEXCategoria);
                    if (vtex_produto.Erros.Mensagem != null && vtex_produto.Erros.Mensagem.Length > 0)
                    {
                        Erros.Mensagem = "Produto categoria similar (" + item.IdVTEXProduto.ToString() + "," + item.IdVTEXCategoria.ToString() + "): " + vtex_produto.Erros.Mensagem;
                        Erros.Numero = -1;
                        Erros.Gravar();
                        vtex_produto.Erros.Limpar();
                        BLL.XPC.XPC_Log.Dal.Incluir(item.IdVTEXProduto, "ProdutoCategoriaSimilar","Produto-importar", Erros.Mensagem);
                        ocorreramErros = true;
                        item.fl_sinalizacao = 2;
                    }
                    else
                    {
                        item.fl_sinalizacao = 1;
                    }
                    cCategoriaProduto.UpdateSinalizacao(item);
                }

                //verificar produtos inativos na vtex que estejam ativos no sistema e ativar

                if (ocorreramErros)
                    throw new Exception("O processo de importação dos produtos foi concluído com erros.");

                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                Erros.Gravar();
                BLL.XPC.XPC_Log.Dal.Incluir("Produto-importar", Erros.Mensagem);
                return false;
            }
        }

        public bool IncluirSku(Model.VTEX_Sku sku)
        {
            try
            {
                
                bool IncluirSKu = false;
                StockKeepingUnitDTO objSku = new StockKeepingUnitDTO();
                objSku.Id = sku.IdVTEXSku;
                //verifica se o sku existe na vtex
                objSku = vtex_sku.Item(objSku, sku.EAN13, sku.codigoReferencia);
                if (vtex_sku.Erros.Numero != 0)
                {
                    Erros.Numero = vtex_sku.Erros.Numero;
                    Erros.Mensagem = "Sku (" + sku.IdVTEXSku.ToString() + "): consulta deu erro ao buscar na vtex. " + vtex_sku.Erros.Mensagem;
                    Erros.Gravar();
                    BLL.XPC.XPC_Log.Dal.Incluir(sku.IdVTEXSku, "Sku", "Produto-incluirSku", Erros.Mensagem);
                    vtex_sku.Erros.Limpar();
                    return false;
                }else
                {
                    IncluirSKu = (objSku.Id == null);
                }
                //inclui/alterar o sku
                if (IncluirSKu)
                {
                    objSku.Id = sku.IdVTEXSku;
                }
                
                //acertos de base
                if (sku.precoDe == null && sku.precoPor == null)
                {
                    sku.precoDe = sku.precoCusto;
                    sku.precoPor = sku.precoCusto;
                }
                if (sku.fl_sinalizacao == 0)
                {


                    objSku.ProductId = sku.IdVTEXProduto;
                    objSku.IsActive = sku.fl_ativo;
                    objSku.Name = (sku.sku.Length > 0) ? sku.sku : sku.VTEX_Produto.produto;
                    objSku.RefId = sku.codigoReferencia;
                    objSku.CostPrice = sku.precoCusto;
                    objSku.ListPrice = sku.precoDe;
                    objSku.Price = sku.precoPor;
                    objSku.Height = sku.altura;
                    objSku.Length = sku.comprimento;
                    objSku.Width = sku.largura;
                    objSku.WeightKg = sku.peso;
                    objSku.ProductName = sku.VTEX_Produto.produto;
                    objSku.ManufacturerCode = sku.codigoFabricante;
                    objSku.EstimatedDateArrival = sku.preVenda;
                    objSku.IsKit = sku.VTEX_SkuKitPai.Count > 0;

                    StockKeepingUnitEanDTO[] objSkuEan = null;
                    if (sku.EAN13 != null && sku.EAN13.Trim().Length > 0)
                    {
                        objSkuEan = new StockKeepingUnitEanDTO[1];
                        objSkuEan[0] = new StockKeepingUnitEanDTO();
                        objSkuEan[0].Ean = sku.EAN13;
                    }
                    if (IncluirSKu || objSkuEan != null)
                        objSku.StockKeepingUnitEans = objSkuEan;

                    objSku = vtex_sku.InsertUpdate(objSku);
                    if (objSku == null || vtex_sku.Erros.Numero != 0)
                    {
                        throw new Exception("Sku Insert/Update (" + sku.IdVTEXSku.ToString() + "): " + vtex_sku.Erros.Mensagem);
                    }
                }

                RestSharp.IRestResponse response;

                if (sku.fl_sinalizacao == 0 || sku.fl_sinalizacao == 10 || sku.fl_sinalizacao == 11)
                {
                    
                    //insiro o estoque
                    VTEX.API.Raml.Logistics apiLogistica = new VTEX.API.Raml.Logistics(Loja, AppKey, AppToken);
                    response = apiLogistica.UpdateInventory((int)objSku.Id, ConfigurationSettings.AppSettings["idEstoquePadrao"].ToString(), ((sku.estoqueInfinito) ? 999999 : sku.estoque));
                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        Erros.Numero = -1;
                        Erros.Mensagem = "Estoque Inclusão de sku(" + sku.IdVTEXSku + "): " + response.StatusDescription + " - " + response.ErrorMessage;
                        Erros.Gravar();
                        BLL.XPC.XPC_Log.Dal.Incluir(sku.IdVTEXSku, "SkuEstoque", "Produto-incluirSku-estoque", Erros.Mensagem);
                    }
                }
                if (sku.fl_sinalizacao == 0 || sku.fl_sinalizacao == 10 || sku.fl_sinalizacao == 12)
                {
                    //para o saleschannel 1 atualizar preço apenas se for uma atualização de produto. A inclusão do preço na inclusão do produto é no mesmo processo de incluir o sku.
                    if (!IncluirSKu)
                    {
                        if (!vtex_sku.StockKeepingUnitPriceUpdate(sku, 1, true))
                        {
                            Erros = vtex_sku.Erros;
                            Erros.Gravar();
                            BLL.XPC.XPC_Log.Dal.Incluir(sku.IdVTEXSku, "SkuPreco", "Produto-incluirSku-preco", Erros.Mensagem);
                        }
                    }                                       
                   
                    if (sku.precoDe2 != null && sku.precoDe2 > 0)
                    {
                        if (!vtex_sku.StockKeepingUnitPriceUpdate(sku, 2, false))
                        {
                            Erros = vtex_sku.Erros;
                            Erros.Gravar();
                            BLL.XPC.XPC_Log.Dal.Incluir(sku.IdVTEXSku, "SkuPreco", "Produto-incluirSku-preco", Erros.Mensagem);
                        }
                    }
                    if (sku.precoDe3 != null && sku.precoDe3 > 0)
                    {
                        if (!vtex_sku.StockKeepingUnitPriceUpdate(sku, 3, false))
                        {
                            Erros = vtex_sku.Erros;
                            Erros.Gravar();
                            BLL.XPC.XPC_Log.Dal.Incluir(sku.IdVTEXSku, "SkuPreco", "Produto-incluirSku-preco", Erros.Mensagem);
                        }
                    }
                }

                /*
                if (Erros.Numero != 0)
                {
                    sku.fl_sinalizacao = 2;
                }else
                {
                    sku.fl_sinalizacao = 1;
                }
                */
                sku.fl_sinalizacao = 1;
                cSku.UpdateSinalizacao(sku);

                //incluir imagens do sku
                ERP_VTEX.Vtex.SkuImagem skuImagem = new SkuImagem(Loja, AppKey, AppToken, UserSoap, PassSoap);
                if (!skuImagem.IncluirImagem(sku)){
                    Erros.Mensagem = "importar imagem sku(" + sku.IdVTEXSku.ToString() + "): " + vtex_imagens.Erros.Mensagem;
                    Erros.Numero = vtex_imagens.Erros.Numero;
                    vtex_imagens.Erros.Limpar();
                    Erros.Gravar();
                    BLL.XPC.XPC_Log.Dal.Incluir(sku.IdVTEXSku, "SkuImage", "Produto-incluirSku-imagem", Erros.Mensagem);
                }
                
                //incluir especificações do sku
                List<Model.VTEX_SkuEspecificacao> skuespecificacoes = cSkuEspecificacao.Lista(sku.IdVTEXSku);
                foreach(Model.VTEX_SkuEspecificacao especificacao in skuespecificacoes)
                {
                    if (!vtex_sku.StockKeepingUnitEspecificationInsert(sku.IdVTEXSku, especificacao.campoNome, especificacao.campoValor.Split('¨')))
                    {
                        Erros.Mensagem = "Marcar Especificação sku(" + sku.IdVTEXSku.ToString() + "): " + vtex_sku.Erros.Mensagem;
                        Erros.Numero = vtex_sku.Erros.Numero;
                        vtex_sku.Erros.Limpar();
                        Erros.Gravar();
                        BLL.XPC.XPC_Log.Dal.Incluir(sku.IdVTEXSku, "SkuEspecificacao", "Produto-incluirSku-especificacao", Erros.Mensagem);
                    }
                }

                //configurar como kit
                sku = vtex_sku.IncluirKit(sku);
                if (vtex_sku.Erros.Numero != 0)
                {
                    Erros.Mensagem = "Incluir Kit (" + sku.IdVTEXSku.ToString() + "): " + vtex_sku.Erros.Mensagem;
                    Erros.Numero = vtex_sku.Erros.Numero;
                    vtex_sku.Erros.Limpar();
                    Erros.Gravar();
                    BLL.XPC.XPC_Log.Dal.Incluir(sku.IdVTEXSku, "Sku", "Produto-incluirSku-kit", Erros.Mensagem);
                    sku.fl_sinalizacao = 2;//erro
                    cSku.UpdateSinalizacao(sku);
                }

                if (!vtex_sku.StockKeepUnitActive(sku.IdVTEXSku))
                {
                    Erros.Mensagem = "Ativar SKu (" + sku.IdVTEXSku.ToString() + "): " + vtex_sku.Erros.Mensagem;
                    Erros.Numero = vtex_sku.Erros.Numero;
                    vtex_sku.Erros.Limpar();
                    Erros.Gravar();
                    BLL.XPC.XPC_Log.Dal.Incluir(sku.IdVTEXSku, "Sku", "Produto-incluirSku-Ativar", Erros.Mensagem);
                    sku.fl_sinalizacao = 2;//erro
                    cSku.UpdateSinalizacao(sku);
                }

                return true;
            }catch(Exception ex)
            {
                int idSku = 0;
                if(sku != null)
                {
                    idSku = sku.IdVTEXSku;
                    sku.fl_sinalizacao = 2;//erro
                    cSku.UpdateSinalizacao(sku);
                }
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "IncluirSku (" + idSku.ToString() + "): " + ex.Message;
                Erros.Gravar();
                vtex_sku.Erros.Limpar();
                BLL.XPC.XPC_Log.Dal.Incluir(idSku, "Sku", "Produto-incluirSku", Erros.Mensagem);
                return false;
            }
        }

        public bool AtivarSkus()
        {
            try
            {
                List<Model.VTEX_Sku> skus = cSku.ListaStatus(1).Where(x=>x.fl_ativo == true).ToList();
                foreach (Model.VTEX_Sku sku in skus)
                {
                    if (sku.fl_ativo)
                    {
                        if (!vtex_sku.StockKeepUnitActive(sku.IdVTEXSku))
                        {
                            Erros.Mensagem = "Ativar SKu (" + sku.IdVTEXSku.ToString() + "): " + vtex_sku.Erros.Mensagem;
                            Erros.Numero = vtex_sku.Erros.Numero;
                            vtex_sku.Erros.Limpar();
                            Erros.Gravar();
                            BLL.XPC.XPC_Log.Dal.Incluir(sku.IdVTEXSku, "Sku", "Sku-Ativar", Erros.Mensagem);
                            sku.fl_sinalizacao = 2;//erro
                            cSku.UpdateSinalizacao(sku);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "AtivarSkus: " + ex.Message;
                Erros.Gravar();
                BLL.XPC.XPC_Log.Dal.Incluir("Produto-AtivarSku", Erros.Mensagem);
                return false;
            }
        }
    }
}
