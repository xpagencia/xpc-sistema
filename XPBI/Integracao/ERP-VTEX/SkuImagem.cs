﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIGlobal;
using Newtonsoft.Json.Linq;
using VTEX.Webservice.WSDL;

namespace XPBI.Integracao.ERP_VTEX.Vtex
{
    public class SkuImagem : Configuracao
    {
        private Integracao.VTex.Imagens vtex_imagens;
        private BLL.VTEX.VTEX_SkuImagem.Dal cSkuImagem = new BLL.VTEX.VTEX_SkuImagem.Dal();

        public SkuImagem(string loja, string appKey, string appToken, string userSoap, string passSoap)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            UserSoap = userSoap;
            PassSoap = passSoap;
            vtex_imagens = new Integracao.VTex.Imagens(Loja, UserSoap, PassSoap);
        }

        public SkuImagem(string loja)
        {
            Loja = loja;
        }

        /// <summary>
        /// Atualiza as informações de categorias dentro da VTEX
        /// </summary>
        /// <returns></returns>
        public bool Importar()
        {
            try
            {
                Integracao.VTex.Sku vtex_sku = new Integracao.VTex.Sku(Loja, AppKey, AppToken, UserSoap, PassSoap);
                BLL.VTEX.VTEX_Sku.Dal cSku = new BLL.VTEX.VTEX_Sku.Dal();

                List<Model.VTEX_Sku> skus = cSkuImagem.ListaStatus(0).Select(x => x.VTEX_Sku).OrderBy(x => x.IdVTEXProduto).ToList();
                foreach (Model.VTEX_Sku sku in skus)
                {
                    if (!IncluirImagem(sku))
                    {
                        BLL.XPC.XPC_Log.Dal.Incluir(sku.IdVTEXSku, "SkuImage", "Imagem-importar", Erros.Mensagem);
                        if (!vtex_sku.StockKeepUnitActive(sku.IdVTEXSku))
                        {
                            Erros.Mensagem = "Ativar SKu (" + sku.IdVTEXSku.ToString() + "): " + vtex_sku.Erros.Mensagem;
                            Erros.Numero = vtex_sku.Erros.Numero;
                            vtex_sku.Erros.Limpar();
                            Erros.Gravar();
                            BLL.XPC.XPC_Log.Dal.Incluir(sku.IdVTEXSku, "Sku", "Imagem-Importar-AtivarSku", Erros.Mensagem);
                            sku.fl_sinalizacao = 2;//erro
                            cSku.UpdateSinalizacao(sku);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "Importar Imagens: " + ex.Message;
                Erros.Gravar();
                BLL.XPC.XPC_Log.Dal.Incluir("Imagem-importar", Erros.Mensagem);
                return false;
            }
        }

        public bool IncluirImagem(Model.VTEX_Sku sku)
        {
            try
            {
                vtex_imagens.Erros.Limpar();
                //verifica se tem alguma fl_sinalizacao = 0 para gerar manutenção. Se não tem, então ignora imagens.
                if (sku.VTEX_SkuImage.Where(x => x.fl_sinalizacao == 0).Count() > 0)
                {
                    bool reiniciarImgs = false;
                    bool imagensVtexExiste = true;
                    //baixar da vtex todas as imagens do sku
                    ImageDTO[] imagensVtex = vtex_imagens.ImageListByStockKeepingUnitId(sku.IdVTEXSku);
                    if(imagensVtex == null || vtex_imagens.Erros.Mensagem != null && vtex_imagens.Erros.Mensagem.Length > 0)
                    {
                        if((imagensVtex == null && (vtex_imagens.Erros.Mensagem == null || vtex_imagens.Erros.Mensagem.Length == 0)) || vtex_imagens.Erros.Mensagem.IndexOf("verificar mensagem") > -1)
                        {
                            reiniciarImgs = true;
                            if (imagensVtex == null && (vtex_imagens.Erros.Mensagem == null || vtex_imagens.Erros.Mensagem.Length == 0))
                            {
                                imagensVtexExiste = false;
                            }
                        }
                        else
                        {
                            throw new Exception(vtex_imagens.Erros.Mensagem);
                        }                        
                    }
                    if (!reiniciarImgs)
                    {
                        foreach (ImageDTO imagemVtex in imagensVtex)
                        {
                            //verificar se tem imagem na vtex que nao tem na base. Se sim, apaga todas as imagens do sku na vtex e sobe da base.
                            //verificar se tem imagem na vtex que tem na base e está com fl_sinalizacao = 0. Se sim, apaga todas as imagens do sku na vtex e sobe da base.
                            if (sku.VTEX_SkuImage.Where(x => x.name == imagemVtex.Name).Count() == 0 || sku.VTEX_SkuImage.Where(x => x.name == imagemVtex.Name && x.fl_sinalizacao == 0).Count() > 0)
                            {
                                reiniciarImgs = true;
                                break;
                            }
                        }
                    }                    

                    if (reiniciarImgs && imagensVtexExiste)
                    {
                        vtex_imagens.StockKeepingUnitImageRemove(sku.IdVTEXSku);
                    }

                    foreach (Model.VTEX_SkuImage imagem in sku.VTEX_SkuImage)
                    {
                        if (imagem.fl_sinalizacao == 0 || reiniciarImgs)
                        {
                            ImageDTO imageDTO = new ImageDTO();
                            imageDTO.IsMain = imagem.fl_principal;
                            imageDTO.Label = imagem.label;
                            imageDTO.StockKeepingUnitId = imagem.IdSku;
                            imageDTO.Tag = imagem.tag;
                            imageDTO.Url = imagem.urlOriginal;
                            if (vtex_imagens.InsertUpdate(imageDTO) == null)
                            {
                                Erros.Mensagem = "Imagem (" + imagem.IdSkuImage.ToString() + ") do Sku (" + sku.IdVTEXSku.ToString() + "): " + vtex_imagens.Erros.Mensagem;
                                Erros.Numero = vtex_imagens.Erros.Numero;
                                vtex_imagens.Erros.Limpar();
                                Erros.Gravar();
                                BLL.XPC.XPC_Log.Dal.Incluir(sku.IdVTEXSku, "SkuImage", "sku-Imagem", Erros.Mensagem);
                                imagem.fl_sinalizacao = 2;
                                cSkuImagem.UpdateSinalizacao(imagem);
                            }
                            else
                            {
                                imagem.fl_sinalizacao = 1;
                                cSkuImagem.UpdateSinalizacao(imagem);
                            }
                        }
                    }
                }
                return true;
            }catch(Exception ex)
            {
                Erros.Mensagem = "Importar imagem (" + sku.IdVTEXSku.ToString() + "): " + ex.Message;
                Erros.Numero = ex.HResult;
                Erros.Gravar();
                List<Model.VTEX_SkuImage> imagens = sku.VTEX_SkuImage.Where(x => x.fl_sinalizacao == 0).ToList(); 
                foreach(Model.VTEX_SkuImage imagem in imagens)
                {
                    imagem.fl_sinalizacao = 2;
                    cSkuImagem.UpdateSinalizacao(imagem);
                }
                return false;
            }

        }
    }
}
