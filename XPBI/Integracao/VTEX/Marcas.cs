﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using VTEX.API.Raml;
using VTEX.Webservice.WSDL;
using VTEX.Webservice;

namespace XPBI.Integracao.VTex
{
    public class Marcas: Configuracao
    {
        public Marcas(string loja, string appKey, string appToken)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken; 
        }

        public Marcas(string loja, string appKey, string appToken, string userSOAP, string passSoap)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            UserSoap = userSOAP;
            PassSoap = passSoap;
        }

        public JArray Lista()
        {
            try
            {
                Catalog_system catalogSystem = new Catalog_system(Loja, AppKey, AppToken);
                IRestResponse response = catalogSystem.Category(5);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    return JArray.Parse(response.Content);
                else
                    throw new Exception(response.ErrorMessage);
            }catch(Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return JArray.Parse("[]");
            }
        }

        public JObject Item(long id)
        {
            JArray lista = Lista();
            foreach(JObject item in lista)
            {
                if(Convert.ToDecimal(item["id"]) == id)
                    return item;
            }
            return JObject.Parse("{}");
        }

        public BrandDTO InsertUpdate(BrandDTO _obj)
        {
            try
            {
                BrandDTO tmp;
                Marca itemSoap = new Marca();
                itemSoap.AccountName = Loja;
                itemSoap.UserSOAP = UserSoap;
                itemSoap.PassSOAP = PassSoap;
                tmp = itemSoap.InsertUpdate(_obj);
                if(tmp == null && _obj.Id == null && itemSoap.Erros.Mensagem == "Data is Null. This method or property cannot be called on Null values.")
                {
                    //não tem nenhuma categoria na loja. É necessário incluir o Id manual.
                    _obj.Id = 1;
                    tmp = itemSoap.InsertUpdate(_obj);
                }
                return tmp;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return null;
            }
        }
    }
}
