﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using VTEX.API.Raml;
using VTEX.Webservice.WSDL;
using VTEX.Webservice;

namespace XPBI.Integracao.VTex
{
    public class Produtos: Configuracao
    {
        public Produtos(string loja, string appKey, string appToken)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken; 
        }

        public Produtos(string loja, string appKey, string appToken, string userSOAP, string passSoap)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            UserSoap = userSOAP;
            PassSoap = passSoap;
        }
        
        public JArray Lista()
        {
            try
            {
                Catalog_system catalogSystem = new Catalog_system(Loja, AppKey, AppToken);
                IRestResponse response = catalogSystem.Category(5);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    return JArray.Parse(response.Content);
                else
                    throw new Exception(response.ErrorMessage);
            }catch(Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return JArray.Parse("[]");
            }
        }

        public JObject Item(long id)
        {
            try
            {
                Catalog_system catalogSystem = new Catalog_system(Loja, AppKey, AppToken);
                IRestResponse response = catalogSystem.ProductById(id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    return JObject.Parse(response.Content);
                else
                    throw new Exception(response.ErrorMessage);
            }catch(Exception ex){
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return JObject.Parse("{}");
            }            
        }

        public JObject GetProductVariations(int id)
        {
            try
            {
                Catalog_system catalogSystem = new Catalog_system(Loja, AppKey, AppToken);
                IRestResponse response = catalogSystem.GetProductVariations(id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    return JObject.Parse(response.Content);
                else
                    throw new Exception(response.StatusDescription + " " +  response.ErrorMessage);
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return JObject.Parse("{}");
            }
        }

        public JArray GetAllProductVariations(int id)
        {
            try
            {
                JArray retorno = new JArray();
                VTEX.Webservice.Sku skuSoap = new VTEX.Webservice.Sku();
                skuSoap.AccountName = Loja;
                skuSoap.UserSOAP = UserSoap;
                skuSoap.PassSOAP = PassSoap;
                StockKeepingUnitDTO[] skus = skuSoap.StockKeepingUnitGetAllByProduct(id);
                if (skus == null && skuSoap.Erros.Mensagem.Length > 0)
                {
                    Erros = skuSoap.Erros;
                }else
                {
                    retorno = JArray.FromObject(skus);
                }
                return retorno;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return JArray.Parse("[]");
            }
        }
        

        public ProductDTO InsertUpdate(ProductDTO _objProduct)
        {
            try
            {
                ProductDTO retorno = new ProductDTO();
                Produto produtoSoap = new Produto();
                produtoSoap.AccountName = Loja;
                produtoSoap.UserSOAP = UserSoap;
                produtoSoap.PassSOAP = PassSoap;
                retorno = produtoSoap.InsertUpdate(_objProduct);
                if(retorno == null && produtoSoap.Erros.Mensagem.Length > 0)
                {
                    Erros = produtoSoap.Erros;
                }
                return retorno;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return null;
            }
        }

        public void ProductSetSimilarCategory(int idProduto, int idCategoria)
        {
            try
            {
                Produto produtoSoap = new Produto();
                produtoSoap.AccountName = Loja;
                produtoSoap.UserSOAP = UserSoap;
                produtoSoap.PassSOAP = PassSoap;
                produtoSoap.ProductSetSimilarCategory(idProduto, idCategoria);
                if (produtoSoap.Erros.Mensagem != null && produtoSoap.Erros.Mensagem.Length > 0)
                {
                    Erros = produtoSoap.Erros;
                }
            }catch(Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
            }
        }

        public bool ProductEspecificationInsert(int idSku, string fieldName, string[] fieldValues)
        {
            try
            {
                VTEX.Webservice.Produto soap = new VTEX.Webservice.Produto();
                soap.AccountName = Loja;
                soap.UserSOAP = UserSoap;
                soap.PassSOAP = PassSoap;
                soap.ProductEspecificationInsert(idSku, fieldName, fieldValues);
                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "VTEX-WS: (" + idSku.ToString() + ") - " + ex.Message;
                return false;
            }
        }

        public bool ProductActive(int idProduto)
        {
            try
            {
                VTEX.Webservice.Produto soap = new VTEX.Webservice.Produto();
                soap.AccountName = Loja;
                soap.UserSOAP = UserSoap;
                soap.PassSOAP = PassSoap;
                soap.ProductActive(idProduto);
                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "VTEX-WS: ProductActive (" + idProduto.ToString() + ") - " + ex.Message;
                return false;
            }

        }

    }
}
