﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using VTEX.API.Raml;
using VTEX.Webservice.WSDL;
using VTEX.Webservice;

namespace XPBI.Integracao.VTex
{
    public class Categorias: Configuracao
    {
        public Categorias(string loja, string appKey, string appToken)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken; 
        }

        public Categorias(string loja, string appKey, string appToken, string userSOAP, string passSoap)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            UserSoap = userSOAP;
            PassSoap = passSoap;
        }

        public JArray Lista()
        {
            try
            {
                Catalog_system catalogSystem = new Catalog_system(Loja, AppKey, AppToken);
                IRestResponse response = catalogSystem.Category(5);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    return JArray.Parse(response.Content);
                else
                    throw new Exception(response.ErrorMessage);
            }catch(Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return JArray.Parse("[]");
            }
        }

        public JObject Item(long id)
        {
            return Item(id, null);
        }

        public JObject Item(long id, JArray categorias)
        {
            if(categorias == null)
                categorias = Lista();

            foreach(JObject categoria in categorias)
            {
                if (Convert.ToDecimal(categoria["id"]) == id)
                    return categoria;

                if (categoria["children"].ToString().Length > 0)
                {
                    JObject res = Item(id, JArray.FromObject(categoria["children"]));
                    if (res.Count > 0) return res;
                }
                
            }
            return JObject.Parse("{}");
        }

        public CategoryDTO InsertUpdate(CategoryDTO _objCategory)
        {
            try
            {
                CategoryDTO tmp;
                Categoria categoriaSoap = new Categoria();
                categoriaSoap.AccountName = Loja;
                categoriaSoap.UserSOAP = UserSoap;
                categoriaSoap.PassSOAP = PassSoap;
                tmp = categoriaSoap.InsertUpdate(_objCategory);
                if(tmp == null && categoriaSoap.Erros.Numero != 0)
                {
                    Erros = categoriaSoap.Erros;
                }
                return tmp;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return null;
            }
        }

        
    }
}
