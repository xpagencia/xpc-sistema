﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIGlobal;

namespace XPBI.Integracao.VTex
{
    public class Configuracao
    {
        public IErros Erros = new Erros();
        public string Loja { get; set; }
        public string AppKey { get; set; }
        public string AppToken { get; set; }
        public string UserSoap { get; set; }
        public string PassSoap { get; set; }

    }
}
