﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using VTEX.API.Raml;
using VTEX.Webservice.WSDL;
using VTEX.Webservice;

namespace XPBI.Integracao.VTex.OMS
{
    public class Pedido : Configuracao
    {
        private VTEX.API.Raml.OMS apiOMS;

        public Pedido(string loja, string appKey, string appToken)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            apiOMS = new VTEX.API.Raml.OMS(Loja, AppKey, AppToken);
        }

        public Pedido(string loja, string appKey, string appToken, string userSoap, string passSoap)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            UserSoap = userSoap;
            PassSoap = passSoap;
            apiOMS = new VTEX.API.Raml.OMS(Loja, AppKey, AppToken);
        }

        /// <summary>
        /// importa para a XP todos os pedidos pendentes
        /// </summary>
        /// <returns></returns>
        public bool Importar()
        {
            bool existePedido = true;
            try
            {
                //baixa o pedido pendente do feed e atualiza a vtex de que o pedido ja foi baixado para a XP
                while (existePedido)
                {
                    IRestResponse response = apiOMS.GetFeedOrderStatus(10);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.Content != "null")
                        {
                            JArray dados = JArray.Parse(response.Content);
                            if (dados.Count > 0)
                            {
                                BLL.VTEX.Pedido.Dal VTEXPedidoDal = new BLL.VTEX.Pedido.Dal();
                                BLL.VTEX.OMS_Feed.Dal OMS_Feed = new BLL.VTEX.OMS_Feed.Dal();

                                foreach (JObject item in dados)
                                {
                                    bool commitFeed = true;

                                    Model.VTEX_feed itemFeed = new Model.VTEX_feed();
                                    itemFeed.orderId = item["orderId"].ToString();
                                    itemFeed.status = item["status"].ToString();
                                    itemFeed.dateTime = Convert.ToDateTime(item["dateTime"]);
                                    itemFeed.commitToken = item["commitToken"].ToString();

                                    switch (itemFeed.status)
                                    {
                                        /*
                                         
                                         */
                                        case "payment-pending":
                                        //case "order-created":
                                        case "authorize-fulfillment":
                                        case "ready-for-handling":
                                        case "cancel":
                                        case "canceled":
                                        case "ship / invoice":
                                        case "window-to-cancel":
                                        case "handling":
                                        case "approve-payment":
                                        case "invoiced":
                                            commitFeed = false;
                                            //verifica se existe registro mais atual para o orderId ja exportado

                                            if (!OMS_Feed.Exists(itemFeed))
                                            {
                                                //monta a VTEXPedido
                                                Model.VTEX_Pedido VTEXPedidoItem = new Model.VTEX_Pedido();
                                                //consulta o pedido na vtex para pegar outras informações
                                                VTEXPedidoItem = GetOrder(itemFeed.orderId);
                                                if (VTEXPedidoItem != null)
                                                {
                                                    //grava/atualiza o pedido na XP
                                                    if (VTEXPedidoDal.InsertUpdate(VTEXPedidoItem))
                                                    {
                                                        commitFeed = true;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                commitFeed = true;
                                            }
                                            break;
                                    }
                                    if (commitFeed)
                                    {
                                        //avisar o feed que o pedido foi lido
                                        OMS_Feed.InsertUpdate(itemFeed);
                                        IRestResponse confirm = apiOMS.ConfirmItemFeedOrderStatus(itemFeed.commitToken.Replace("\"", "\\\""));
                                    }
                                }
                            }
                            else
                            {
                                Erros.Numero = 1;
                                Erros.Mensagem = "A leitura da vtex não retornou nenhum pedido pendente.";
                                existePedido = false;
                            }
                        }
                        else
                        {
                            Erros.Numero = 1;
                            Erros.Mensagem = "A leitura da vtex não retornou nenhuma informação.";
                            existePedido = false;
                        }
                    }
                    else
                    {
                        throw new Exception(response.StatusDescription);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                BLL.XPC.XPC_Log.Dal.Incluir("Pedido-Importar", Erros.Mensagem);
                return false;
            }
        }

        public Model.VTEX_Pedido GetOrder(string orderId)
        {
            Model.VTEX_Pedido retorno = new Model.VTEX_Pedido();
            try
            {
                //baixa o pedido pendente do feed e atualiza a vtex de que o pedido ja foi baixado para a XP
                IRestResponse response = apiOMS.GetOrder(orderId);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content != "null")
                    {
                        JObject dados = JObject.Parse(response.Content);
                        if (dados.Count > 0)
                        {
                            retorno.orderId = orderId;
                            retorno.status = dados["status"].ToString();
                            retorno.DataCriacao = Convert.ToDateTime(dados["creationDate"]);
                            retorno.json = response.Content;
                            retorno.dataAlteracao = Convert.ToDateTime(dados["lastChange"]); 
                            retorno.origin = dados["origin"].ToString();
                            retorno.sequence = Convert.ToInt32(dados["sequence"]);
                            retorno.valorTotal = Convert.ToDecimal(dados["value"])/100;

                            JArray totals = JArray.FromObject(dados["totals"]);
                            foreach(JObject total in totals)
                            {
                                switch (total["id"].ToString())
                                {
                                    case "Shipping":
                                        retorno.valorFrete = Convert.ToDecimal(total["value"]) / 100;
                                        break;
                                    case "Discounts":
                                        retorno.valorDesconto = Convert.ToDecimal(total["value"]) / 100;
                                        break;
                                }
                                
                            }
                            

                            Model.VTEX_PedidoCliente cliente = new Model.VTEX_PedidoCliente();
                            JObject clientProfileData = JObject.FromObject(dados["clientProfileData"]);

                            cliente.cpf = clientProfileData["document"].ToString();
                            cliente.email = clientProfileData["email"].ToString();
                            cliente.nome = clientProfileData["firstName"].ToString() + " " + clientProfileData["lastName"].ToString();
                            cliente.telefone = clientProfileData["phone"].ToString();

                            JObject shippingDataAddress = JObject.FromObject(dados["shippingData"]["address"]);
                            cliente.endereco = shippingDataAddress["street"].ToString();
                            cliente.numero = shippingDataAddress["number"].ToString();
                            cliente.complemento = shippingDataAddress["complement"].ToString();
                            cliente.bairro = shippingDataAddress["neighborhood"].ToString();
                            cliente.cidade = shippingDataAddress["city"].ToString();
                            cliente.UF = shippingDataAddress["state"].ToString();
                            cliente.cep = shippingDataAddress["postalCode"].ToString().Replace(".","").Replace("-","").Trim();

                            Clientes clienteMD = new Clientes(Loja, AppKey, AppToken);
                            JObject cl = clienteMD.ItemUserId(clientProfileData["userProfileId"].ToString());
                            if(cl.Count > 0)
                            {
                                cliente.emailStr = cl["email"].ToString();
                            }
                            retorno.VTEX_PedidoCliente.Add(cliente);

                            JArray items = JArray.FromObject(dados["items"]);
                            int itemIndex = -1;
                            foreach(JObject item in items)
                            {
                                itemIndex++;
                                decimal descontoItem = 0;
                                if (item["priceTags"].Count() > 0)
                                {
                                    foreach(JObject priceTagsItem in item["priceTags"])
                                    {
                                        if (!(priceTagsItem["name"].ToString().StartsWith("discount@shipping") && retorno.valorFrete == 0))
                                        {
                                            if (!Convert.ToBoolean(priceTagsItem["isPercentual"]))
                                                descontoItem = (Convert.ToDecimal(priceTagsItem["value"]) * (-1)) / 100;
                                            else
                                            {
                                                descontoItem = (retorno.valorTotal * (Convert.ToDecimal(priceTagsItem["value"]) * (-1)) / 100) / 100;
                                            }
                                        }
                                    }
                                }
                                    
                                Model.VTEX_PedidoSku pedidoSku = new Model.VTEX_PedidoSku();
                                pedidoSku.quantity = Convert.ToInt16(item["quantity"]);
                                pedidoSku.pesoUnitario = Convert.ToDecimal(item["additionalInfo"]["dimension"]["weight"]);
                                pedidoSku.precoUnitario = (((Convert.ToDecimal(item["price"])/100) * pedidoSku.quantity) - descontoItem)/pedidoSku.quantity;
                                pedidoSku.IdVTEXSku = Convert.ToInt32(item["id"]); // id do sku
                                pedidoSku.idEstoque = dados["shippingData"]["logisticsInfo"][itemIndex]["deliveryIds"][0]["warehouseId"].ToString();
                                pedidoSku.idRef = item["refId"].ToString();
                                retorno.VTEX_PedidoSku.Add(pedidoSku);
                            }

                            JObject logisticsInfo = JObject.FromObject((JArray.FromObject(dados["shippingData"]["logisticsInfo"]))[0]);
                            Model.VTEX_PedidoFrete pedidoFrete = new Model.VTEX_PedidoFrete();
                            pedidoFrete.nome = cliente.nome;
                            pedidoFrete.bairro = cliente.bairro;
                            pedidoFrete.cep = cliente.cep;
                            pedidoFrete.cidade = cliente.cidade;
                            pedidoFrete.complemento = cliente.complemento;
                            pedidoFrete.endereco = cliente.endereco;
                            pedidoFrete.numero = cliente.numero;
                            pedidoFrete.UF = cliente.UF;
                            pedidoFrete.valor = Convert.ToDecimal(logisticsInfo["price"]) / 100;
                            pedidoFrete.codigoTransportadora = JObject.FromObject(logisticsInfo["deliveryIds"][0])["courierId"].ToString();
                            pedidoFrete.nomeTransportadora = logisticsInfo["deliveryCompany"].ToString() + "-" + logisticsInfo["selectedSla"].ToString();
                            /*transportadora
                             * jObject trackingHints = JObject.FromObject((JArray.FromObject(dados["shippingData"]["trackingHints"]))[0]);
                             * pedidoFrete.transportadora_nome = trackingHints["courierName"].toString();
                             * pedidoFrete.transportadora_trackingId = trackingHints["trackingId"].toString();
                             * pedidoFrete.transportadora_trackingUrl = trackingHints["trackingUrl"].toString();
                             */
                            retorno.VTEX_PedidoFrete.Add(pedidoFrete);

                            JArray paymentData = JArray.FromObject(dados["paymentData"]["transactions"]);
                            Model.VTEX_PedidoFormaPagamento pedidoPagamento = new Model.VTEX_PedidoFormaPagamento();
                            foreach(JObject transaction in paymentData)
                            {
                                if (Convert.ToBoolean(transaction["isActive"]))
                                {
                                    JArray payments = JArray.FromObject(transaction["payments"]);
                                    foreach(JObject payment in payments)
                                    {
                                        pedidoPagamento.formaPagamento = payment["paymentSystemName"].ToString();
                                        pedidoPagamento.parcelas = Convert.ToInt32(payment["installments"]);
                                        pedidoPagamento.url = payment["url"].ToString();
                                        pedidoPagamento.valor = Convert.ToDecimal(payment["value"]) / 100;
                                        pedidoPagamento.valorParcela = Convert.ToDecimal(payment["referenceValue"]) / 100;
                                        pedidoPagamento.dataVencimento = (payment["dueDate"].ToString() != "") ? Convert.ToDateTime(payment["dueDate"]) : DateTime.Today;
                                        retorno.VTEX_PedidoFormaPagamento.Add(pedidoPagamento);
                                    }
                                }
                                
                            }
                            
                        }
                        else
                        {
                            Erros.Numero = 1;
                            Erros.Mensagem = "A leitura da vtex não retornou nenhum pedido.";
                            retorno = null;
                        }
                    }
                    else
                    {
                        Erros.Numero = 1;
                        Erros.Mensagem = "A leitura da vtex não retornou nenhuma informação.";
                        retorno = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                retorno = null;
            }
            return retorno;
        }

        /// <summary>
        /// Verifica se o status1 é mais recente que o status2 no fluxo da vtex
        /// </summary>
        /// <param name="status1"></param>
        /// <param name="status2"></param>
        /// <returns>0 - iguais; 1 - status1 mais recente; 2 - status2 mais recente.</returns>
        public int CompararStatusMaisRecente(string status1, string status2)
        {
            if (status1 == status2) return 0;
            string[] opcoes = { "waiting-for-seller-confirmation", "payment-pending", "payment-approved", "payment-denied", "waiting-for-seller-decision", "waiting-ffmt-authorization", "authorize-fulfillment", "window-to-cancel", "ready-for-handling", "start-handling", "handling", "request-cancel", "order-accepted", "shipped", "invoiced", "cancel", "canceled" };
            int retorno = (Array.IndexOf(opcoes, status1) > Array.IndexOf(opcoes, status2)) ? 1 : 2;
            return retorno;
        }

        public bool AtualizarStatus(Model.VTEX_Pedido pedido)
        {
            BLL.VTEX.Pedido.Dal cPedido = new BLL.VTEX.Pedido.Dal();
            try
            {
                //http://{{accountName}}.{{environment}}.com.br/api/oms/pvt/orders/{{orderId}}/changestate/ready-for-handling
                VTEX.API.Raml.OMS oms = new VTEX.API.Raml.OMS(Loja, AppKey, AppToken);
                IRestResponse retorno = oms.ChangeOrderState(pedido.orderId, pedido.status);
                if(retorno.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    //atualiza o status no PE                    
                    pedido.fl_sinalizacao = 1;
                    cPedido.UpdateSinalizacao(pedido);
                    return true;
                }
                else
                {
                    throw new Exception(retorno.StatusDescription + " - " + retorno.Content);
                }                
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "Pedido-AtualizarStatus: " + ex.Message;
                BLL.XPC.XPC_Log.Dal.Incluir(pedido.IdVTEXPedido, "ws.AtualizarStatus", "exception", Erros.Mensagem);
                pedido.fl_sinalizacao = 2;
                cPedido.UpdateSinalizacao(pedido);
                return false;
            }
        }

        public bool UpdateTrackingStatus(Model.VTEX_Pedido pedido)
        {
            BLL.VTEX.Pedido.Dal cPedido = new BLL.VTEX.Pedido.Dal();
            try
            {
                VTEX.API.Raml.OMS oms = new VTEX.API.Raml.OMS(Loja, AppKey, AppToken);
                IRestResponse retorno = oms.ChangeOrderState(pedido.orderId, pedido.status);
                if (retorno.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    //atualiza o status no PE                    
                    pedido.fl_sinalizacao = 1;
                    cPedido.UpdateSinalizacao(pedido);
                    return true;
                }
                else
                {
                    throw new Exception(retorno.StatusDescription + " - " + retorno.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "Pedido-UpdateTrackingStatus: " + ex.Message;
                BLL.XPC.XPC_Log.Dal.Incluir(pedido.IdVTEXPedido, "ws.UpdateTrackingStatus", "exception", Erros.Mensagem);
                pedido.fl_sinalizacao = 2;
                cPedido.UpdateSinalizacao(pedido);
                return false;
            }

        }
        

        public bool EnviarNotaFiscal(Model.VTEX_Pedido pedido)
        {
            BLL.VTEX.Pedido.Dal cPedido = new BLL.VTEX.Pedido.Dal();
            string json = "";
            try
            {
                VTEX.API.Raml.OMS oms = new VTEX.API.Raml.OMS(Loja, AppKey, AppToken);
                foreach(Model.VTEX_PedidoInvoice invoice in pedido.VTEX_PedidoInvoice)
                {
                    string itens = "";
                    json = "{"
                    + "\"invoiceNumber\":\"" + invoice.invoiceNumber.ToString() + "\""
                    + ",\"invoiceValue\":\"" + Decimal.Round(Decimal.Round(invoice.invoiceValue, 2) * 100).ToString() + "\""
                    + ",\"invoiceUrl\":\"" + invoice.invoiceUrl + "\""
                    + ",\"issuanceDate\":\"" + invoice.date.ToString("yyyy-MM-dd") + "\""
                    + ",\"trackingNumber\":" + ((invoice.codigoRastreio == null || invoice.codigoRastreio == "") ? "null" : "\"" + invoice.codigoRastreio + "\"")
                    + ",\"trackingUrl\":" + ((invoice.urlRastreio == null || invoice.urlRastreio == "") ? ((invoice.codigoRastreio == null || invoice.codigoRastreio == "") ? "null" : "\"https://clientes.xpagencia.com.br/ferramentas/logistics/transportadoras/rastreio?rastreio=" + invoice.codigoRastreio + "\"") : "\"" + invoice.urlRastreio + "\"")
                    + ",\"type\":\"Output\""
                    + ",\"items\": [";

                    foreach (Model.VTEX_PedidoInvoiceItem item in invoice.VTEX_PedidoInvoiceItem)
                    {
                        itens += "{"
                            + "\"id\": \"" + item.idSku + "\""
                            + ",\"price\":" + (item.price * 100).ToString()
                            + ",\"quantity\":" + item.quantity.ToString()
                            + "}";
                    }
                    json += "]}";

                    IRestResponse retorno = oms.InvoiceNotification(pedido.orderId, json);
                    if (retorno.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        //atualiza o status no PE                    
                        pedido.fl_sinalizacao = 1;
                        cPedido.UpdateSinalizacao(pedido);
                        BLL.XPC.XPC_Log.Dal.Incluir(invoice.IdVTEXPedidoInvoiceId, "VTEX_PEDIDOINVOICEITEM", "json", json);
                        return true;
                    }
                    else
                    {
                        throw new Exception(retorno.StatusDescription + " - " + retorno.ErrorMessage);
                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "Pedido-EnviarNotaFiscal: " + ex.Message;
                BLL.XPC.XPC_Log.Dal.Incluir(pedido.IdVTEXPedido, "ws.EnviarNotaFiscal", "exception", Erros.Mensagem);
                BLL.XPC.XPC_Log.Dal.Incluir(0, "VTEX_PEDIDOINVOICEITEM", "json", json);
                pedido.fl_sinalizacao = 2;
                cPedido.UpdateSinalizacao(pedido);
                return false;
            }
        }

        public bool UpdateTracking(Model.VTEX_Pedido pedido)
        {
            //http://{{accountName}}.{{environment}}.com.br/api/oms/pvt/orders/{{orderId}}/invoice/{{invoiceNumber}}/tracking
            BLL.VTEX.Pedido.Dal cPedido = new BLL.VTEX.Pedido.Dal();
            try
            {
                VTEX.API.Raml.OMS oms = new VTEX.API.Raml.OMS(Loja, AppKey, AppToken);
                foreach (Model.VTEX_PedidoInvoice invoice in pedido.VTEX_PedidoInvoice)
                {
                   IRestResponse retorno = oms.UpdateTrackingStatus(pedido.orderId, invoice.invoiceNumber.ToString(), invoice.codigoRastreio);
                    if (retorno.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        //atualiza o status no PE                    
                        pedido.fl_sinalizacao = 1;
                        cPedido.UpdateSinalizacao(pedido);
                        BLL.XPC.XPC_Log.Dal.Incluir(invoice.IdVTEXPedidoInvoiceId, "VTEX_PEDIDOINVOICEITEM", "tracking", invoice.codigoRastreio);
                        return true;
                    }
                    else
                    {
                        throw new Exception(retorno.StatusDescription + " - " + retorno.ErrorMessage);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "Pedido-UpdateTracking: " + ex.Message;
                BLL.XPC.XPC_Log.Dal.Incluir(pedido.IdVTEXPedido, "ws.UpdateTracking", "exception", Erros.Mensagem);
                pedido.fl_sinalizacao = 2;
                cPedido.UpdateSinalizacao(pedido);
                return false;
            }
        }
        public bool Exportar()
        {
            try
            {
                //verificar no sistema os pedidos que precisam ser exportados.
                BLL.VTEX.Pedido.Dal cPedido = new BLL.VTEX.Pedido.Dal();

                //atualizar status
                List<Model.VTEX_Pedido> pedidos = cPedido.ListaSinalizacao(20);

                //atualizar faturado
                pedidos.AddRange(cPedido.ListaSinalizacao(21));

                //atualizar entregue
                //pedidos.AddRange(cPedido.ListaSinalizacao(22));

                foreach(Model.VTEX_Pedido pedido in pedidos)
                {
                    BLL.XPC.XPC_Log.Dal.Incluir(pedido.IdVTEXPedido, "oms.pedido", "log", "pedido inicio: " + pedido.orderId);
                    //verificar se o pedido do sistema existe online
                    Model.VTEX_Pedido pedidoOMS = GetOrder(pedido.orderId);
                    if (pedidoOMS == null)
                    {
                        Erros.Mensagem = "Exportar pedidos (pedido não existe na VTEX):" + Erros.Mensagem;
                        Erros.Gravar();
                        BLL.XPC.XPC_Log.Dal.Incluir(pedido.IdVTEXPedido, "VTEX_PEDIDO", "Exportar-pedidos", Erros.Mensagem);
                        Erros.Limpar();
                        pedido.fl_sinalizacao = 2;
                        cPedido.UpdateSinalizacao(pedido);
                        continue;
                    }

                    //verifico se o status na vtex é anterior do status na PE.
                    if(CompararStatusMaisRecente(pedido.status, pedidoOMS.status) != 1)
                    {
                        Erros.Mensagem = "Exportar pedidos: Pedido da VTEX é mais recente ou igual.";
                        Erros.Gravar();
                        BLL.XPC.XPC_Log.Dal.Incluir(pedido.IdVTEXPedido, "VTEX_PEDIDO", "Exportar-pedidos", Erros.Mensagem);
                        Erros.Limpar();
                        pedido.fl_sinalizacao = 2;
                        cPedido.UpdateSinalizacao(pedido);
                        continue;
                    }

                    //atualizar o status do pedido
                    switch (pedido.fl_sinalizacao)
                    {
                        case 20: // atualizar status
                            AtualizarStatus(pedido);
                            break;
                        case 21: //se for nota fiscal ou frete, envio as informações do pedido junto com o novo status
                            EnviarNotaFiscal(pedido);
                            break;
                        case 22:
                            if (pedidoOMS.status != "invoiced") EnviarNotaFiscal(pedido);
                            UpdateTracking(pedido);
                            break;
                    }
                    BLL.XPC.XPC_Log.Dal.Incluir(pedido.IdVTEXPedido, "oms.pedido", "log", "pedido OK: " + pedido.orderId);
                }
                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                BLL.XPC.XPC_Log.Dal.Incluir("Pedido.Exportar", Erros.Mensagem);
                return false;
            }

        }
    }
}
