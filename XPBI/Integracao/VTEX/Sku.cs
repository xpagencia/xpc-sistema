﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using VTEX.API.Raml;
using VTEX.Webservice.WSDL;
using VTEX.Webservice;

namespace XPBI.Integracao.VTex
{
    public class Sku: Configuracao
    {
        public Sku(string loja, string appKey, string appToken)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken; 
        }

        public Sku(string loja, string appKey, string appToken, string userSOAP, string passSoap)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            UserSoap = userSOAP;
            PassSoap = passSoap;
        }

        public StockKeepingUnitDTO InsertUpdate(StockKeepingUnitDTO _obj)
        {
            try
            {
                StockKeepingUnitDTO retorno;
                VTEX.Webservice.Sku skuSoap = new VTEX.Webservice.Sku();
                skuSoap.AccountName = Loja;
                skuSoap.UserSOAP = UserSoap;
                skuSoap.PassSOAP = PassSoap;
                retorno = skuSoap.InsertUpdate(_obj);
                if (retorno.Id == null && skuSoap.Erros.Mensagem != "")
                {
                    this.Erros = skuSoap.Erros;
                };
                return retorno;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return new StockKeepingUnitDTO();
            }
        }

        public StockKeepingUnitDTO StockKeepingUnitGetByEanOrRefId(string ean, string refId)
        {
            try
            {
                //verifica se tem o ean. Se retornar null, verificar se tem o refId do sku.
                StockKeepingUnitDTO retorno;
                VTEX.Webservice.Sku skuSoap = new VTEX.Webservice.Sku();
                skuSoap.AccountName = Loja;
                skuSoap.UserSOAP = UserSoap;
                skuSoap.PassSOAP = PassSoap;
                retorno = skuSoap.StockKeepingUnitGetByEan(ean);
                if (retorno != null)
                {
                    if (retorno.Id == null && skuSoap.Erros != null && skuSoap.Erros.Mensagem != "")
                    {
                        this.Erros = skuSoap.Erros;
                    }
                }
                else
                {
                    retorno = skuSoap.StockKeepingUnitGetByRefId(refId);
                    if(retorno == null)
                    {
                        return new StockKeepingUnitDTO(); //sku nao encontrado
                    }
                    else if (retorno.Id == null && skuSoap.Erros != null && skuSoap.Erros.Mensagem != "")
                    {
                        this.Erros = skuSoap.Erros;
                    }
                }
                return retorno;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return new StockKeepingUnitDTO();
            }
        }

        public StockKeepingUnitDTO Item(StockKeepingUnitDTO sku)
        {
            if (sku.Id == null) return new StockKeepingUnitDTO();
            try
            {
                StockKeepingUnitDTO retorno;
                VTEX.Webservice.Sku skuSoap = new VTEX.Webservice.Sku();
                skuSoap.AccountName = Loja;
                skuSoap.UserSOAP = UserSoap;
                skuSoap.PassSOAP = PassSoap;
                retorno = skuSoap.StockKeepingUnitGet((int)sku.Id);
                if (retorno != null)
                {
                    if (retorno.Id == null && skuSoap.Erros != null && skuSoap.Erros.Mensagem != "")
                    {
                        this.Erros = skuSoap.Erros;
                    }
                }
                else
                {
                    if (retorno == null && skuSoap.Erros != null && skuSoap.Erros.Mensagem.Length > 0)
                    {
                        this.Erros = skuSoap.Erros;
                    }
                }
                return retorno;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return new StockKeepingUnitDTO();
            }
        }

        public JObject Item(int id)
        {
            try
            {
                Catalog_system catalogSystem = new Catalog_system(Loja, AppKey, AppToken);
                IRestResponse response = catalogSystem.GetSku(id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    return JObject.Parse(response.Content);
                else
                    throw new Exception(response.ErrorMessage);
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return JObject.Parse("{}");
            }
        }

        public StockKeepingUnitDTO Item(StockKeepingUnitDTO sku, string refId, string ean)
        {
            StockKeepingUnitDTO retorno = Item(sku);
            if(retorno == null || retorno.Id == null)
            {
                retorno = StockKeepingUnitGetByEanOrRefId(ean, refId);
            }
            return retorno;
        }

        public bool StockKeepUnitActive(int idSku)
        {
            try
            {
                VTEX.Webservice.Sku skuSoap = new VTEX.Webservice.Sku();
                skuSoap.AccountName = Loja;
                skuSoap.UserSOAP = UserSoap;
                skuSoap.PassSOAP = PassSoap;
                skuSoap.StockKeepUnitActive(idSku);
                return true;
            }catch(Exception ex){
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "VTEX-WS: (" + idSku.ToString() + ") - " + ex.Message;
                return false;
            }
            
        }
        public bool StockKeepingUnitEspecificationInsert(int idSku, string fieldName, string[] fieldValues)
        {
            try
            {
                VTEX.Webservice.Sku skuSoap = new VTEX.Webservice.Sku();
                skuSoap.AccountName = Loja;
                skuSoap.UserSOAP = UserSoap;
                skuSoap.PassSOAP = PassSoap;
                skuSoap.StockKeepingUnitEspecificationInsert(idSku, fieldName, fieldValues);
                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "VTEX-WS: (" + idSku.ToString() + ") - " + ex.Message;
                return false;
            }

        }
        public FieldDTO[] StockKeepingUnitEspecificationListBySkuId(int idSku)
        {
            FieldDTO[] retorno = null;
            try
            {
                VTEX.Webservice.Sku skuSoap = new VTEX.Webservice.Sku();
                skuSoap.AccountName = Loja;
                skuSoap.UserSOAP = UserSoap;
                skuSoap.PassSOAP = PassSoap;
                retorno = skuSoap.StockKeepingUnitEspecificationListBySkuId(idSku);
                if(retorno == null && skuSoap.Erros.Numero != 0)
                {
                    Erros.Mensagem = "VTEX-WS: (" + idSku.ToString() + "): " + skuSoap.Erros.Mensagem;
                    Erros.Numero = skuSoap.Erros.Numero;

                }
                return retorno;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "VTEX-WS: (" + idSku.ToString() + ") - " + ex.Message;
                return null;
            }
        }

        public Model.VTEX_Sku IncluirKit(Model.VTEX_Sku sku)
        {
            BLL.XPC.XPC_Log.Dal log = new BLL.XPC.XPC_Log.Dal();
            try
            {
                
                //log.Incluir(new Model.XPC_Log { idChave = sku.IdVTEXSku, mensagem = "verifica se é kit", tabela = "skuKit", tipoLog = "sku-incluirkit" });
                if (sku.VTEX_SkuKitPai.Count == 0)
                {
                  //  log.Incluir(new Model.XPC_Log { idChave = sku.IdVTEXSku, mensagem = "não é kit", tabela = "skuKit", tipoLog = "sku-incluirkit" });
                    return sku;
                }

                //se tiver sku incluido para kit, então enviamos o kit
                //log.Incluir(new Model.XPC_Log { idChave = sku.IdVTEXSku, mensagem = "valida o kit", tabela = "skuKit", tipoLog = "sku-incluirkit" });
                //verifica e marca o sku como kit
                VTEX.Webservice.Sku skuSoap = new VTEX.Webservice.Sku();
                skuSoap.AccountName = Loja;
                skuSoap.UserSOAP = UserSoap;
                skuSoap.PassSOAP = PassSoap;
                StockKeepingUnitDTO skuDto = skuSoap.StockKeepingUnitGet(sku.IdVTEXSku);
                if(skuDto.Id == null || skuDto.IsKit != true || skuSoap.Erros.Numero != 0)
                {
                    if(skuSoap.Erros.Numero != 0)
                    {
                        Erros.Mensagem = "VTEX-WS: Sku-kit get WS (" + sku.IdVTEXSku.ToString() + "): " + skuSoap.Erros.Mensagem;
                        Erros.Numero = skuSoap.Erros.Numero;
                        return sku;
                    }
                    if (skuDto.IsKit != true)
                    {
                        //atualiza o sku na vtex marcando como kit
                        skuDto.IsKit = true;
                        skuSoap.InsertUpdate(skuDto);
                        if (skuSoap.Erros.Numero != 0)
                        {
                            Erros.Mensagem = "VTEX-WS: Sku-kit - ativar kit (" + sku.IdVTEXSku.ToString() + "): " + skuSoap.Erros.Mensagem;
                            Erros.Numero = skuSoap.Erros.Numero;
                            return sku;
                        }
                    }
                }
                //enviar componentes
                
                log.Incluir(new Model.XPC_Log { idChave = sku.IdVTEXSku, mensagem = "ler componentes", tabela = "skuKit", tipoLog =  "sku-incluirkit"});
                if (skuSoap.StockKeepingUnitKitDeleteByParent(sku.IdVTEXSku))
                {
                    foreach (Model.VTEX_SkuKit item in sku.VTEX_SkuKitPai)
                    {
                        StockKeepingUnitKitDTO kit = new StockKeepingUnitKitDTO();
                        kit.UnitPrice = (item.preco == 0) ? item.VTEX_Sku.precoPor : item.preco;
                        kit.Amount = (item.quantidade == 0) ? 1 : item.quantidade;
                        kit.StockKeepingUnitParent = item.IdSkuPai;
                        kit.StockKeepingUnitId = item.IdSku;
                        kit.Id = item.IdVTEXSkuKit;
                        StockKeepingUnitKitDTO retorno = skuSoap.StockKeepingUnitKitInsertUpdate(kit);
                        if (retorno == null && skuSoap.Erros.Numero != 0)
                        {
                            Erros.Mensagem += "VTEX-WS: Sku-kit (" + sku.IdVTEXSku.ToString() + "): " + skuSoap.Erros.Mensagem;
                            Erros.Numero = skuSoap.Erros.Numero;
                            skuSoap.Erros.Limpar();
                        }
                        else
                        {
                            log.Incluir(new Model.XPC_Log { idChave = item.IdSku, mensagem = "componente enviado sku(" + sku.IdVTEXSku.ToString() + ") para o componente (" + item.IdSku.ToString() + ")", tabela = "skuKit", tipoLog = "sku-incluirkit" });
                        }
                    }
                }else
                {
                    Erros.Mensagem += "VTEX-WS: Sku-kit remover(" + sku.IdVTEXSku.ToString() + "): " + skuSoap.Erros.Mensagem;
                    Erros.Numero = skuSoap.Erros.Numero;
                    skuSoap.Erros.Limpar();
                }
              //  log.Incluir(new Model.XPC_Log { idChave = sku.IdVTEXSku, mensagem = "termina envio", tabela = "skuKit", tipoLog = "sku-incluirkit" });
                return sku;   
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "VTEX-WS: SKU-KIT (" + sku.IdVTEXSku.ToString() + ") - " + ex.Message;
                return sku;
            }
        }

        public bool StockKeepingUnitPriceUpdate(Model.VTEX_Sku sku, int salesChannel, bool excluirAntigos)
        {
            try
            {
                decimal precoDe = Convert.ToDecimal(sku.precoDe);
                decimal precoPor = Convert.ToDecimal(sku.precoPor);

                //definir precode e precopor do saleschannel que deseja salvar.
                switch (salesChannel)
                {
                    case 1:
                        precoDe = Convert.ToDecimal(sku.precoDe);
                        precoPor = Convert.ToDecimal(sku.precoPor);
                        break;
                    case 2:
                        precoDe = Convert.ToDecimal(sku.precoDe2);
                        precoPor = Convert.ToDecimal(sku.precoPor2);
                        break;
                    case 3:
                        precoDe = Convert.ToDecimal(sku.precoDe3);
                        precoPor = Convert.ToDecimal(sku.precoPor3);
                        break;
                    default:
                        throw new Exception("Sales channel informado errado.");
                }
                VTEX.API.Raml.Pricing apiPricing = new VTEX.API.Raml.Pricing(Loja, AppKey, AppToken);
                RestSharp.IRestResponse response;
                if (excluirAntigos)
                {
                    //apagar todos os precos que existem na vtex para poder atualizar 
                    response = apiPricing.DelDeletePriceBySkuId((int)sku.IdVTEXSku);
                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                        throw new Exception(response.ErrorMessage);
                }               

                //incluir ou salvar o preço no saleschannel informado
                response = apiPricing.PostSavePrice((int)sku.IdVTEXSku, precoPor, precoDe, salesChannel);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new Exception(response.ErrorMessage);
                }

                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = "VTEX-WS: StockKeepingUnitPriceUpdate(" + sku.IdVTEXSku.ToString() + ") - " + ex.Message;
                return false;
            }
        }
    }
}
