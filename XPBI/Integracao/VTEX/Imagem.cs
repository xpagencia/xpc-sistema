﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using VTEX.API.Raml;
using VTEX.Webservice.WSDL;
using VTEX.Webservice;

namespace XPBI.Integracao.VTex
{
    public class Imagens: Configuracao
    {
        public Imagens(string loja, string userSOAP, string passSoap)
        {
            Loja = loja;
            UserSoap = userSOAP;
            PassSoap = passSoap;
        }
        
        public ImageDTO InsertUpdate(ImageDTO _item)
        {
            try
            {
                ImageDTO retorno = new ImageDTO();
                Imagem oSoap = new Imagem();
                oSoap.AccountName = Loja;
                oSoap.UserSOAP = UserSoap;
                oSoap.PassSOAP = PassSoap;
                retorno = oSoap.InsertUpdate(_item);
                if(retorno == null && oSoap.Erros.Mensagem.Length > 0)
                {
                    Erros = oSoap.Erros;
                }
                return retorno;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return null;
            }
        }

        public ImageDTO[] ImageListByStockKeepingUnitId(int idSku)
        {
            try
            {
                ImageDTO[] retorno;
                Imagem oSoap = new Imagem();
                oSoap.AccountName = Loja;
                oSoap.UserSOAP = UserSoap;
                oSoap.PassSOAP = PassSoap;
                retorno = oSoap.ImageListByStockKeepingUnitId(idSku);
                if (retorno == null)
                {
                    if(oSoap.Erros.Mensagem != null && oSoap.Erros.Mensagem.Length > 0)
                    {
                        Erros = oSoap.Erros;
                    }
                }
                return retorno;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return null;
            }
        }

        public bool StockKeepingUnitImageRemove(int idSKu)
        {
            try
            {
                Imagem oSoap = new Imagem();
                oSoap.AccountName = Loja;
                oSoap.UserSOAP = UserSoap;
                oSoap.PassSOAP = PassSoap;
                if(!oSoap.StockKeepingUnitImageRemove(idSKu))
                {
                    Erros = oSoap.Erros;
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return false;
            }
        }
    }
}
