﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using VTEX.API.Raml;
using VTEX.Webservice.WSDL;
using VTEX.Webservice;

namespace XPBI.Integracao.VTex
{
    public class Clientes: Configuracao
    {
        private VTEX.Masterdata.Raml md;

        public Clientes(string loja, string appKey, string appToken)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            md = new VTEX.Masterdata.Raml(Loja, "CL", appKey, AppToken);
        }

        public JArray Lista(List<Parameter> body, int pos, int qtd)
        {
            try
            {
                
                List<Parameter> headers = new List<Parameter>();
                Parameter param1 = new Parameter();
                param1.Name = "resources";
                param1.Value = pos + "-" + (pos + qtd).ToString();
                param1.Type = ParameterType.HttpHeader;
                headers.Add(param1);

                IRestResponse response = md.Search(headers, body);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    return JArray.Parse(response.Content);
                else
                    throw new Exception(response.ErrorMessage);
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return JArray.Parse("[]");
            }
        }
        
        public JObject ItemUserId(string userId)
        {
            List<Parameter> body = new List<Parameter>();
            Parameter param1 = new Parameter();
            param1.Name = "userId";
            param1.Value = userId;
            param1.Type = ParameterType.QueryString;
            body.Add(param1);
            JArray lista = Lista(body, 0, 1);
            foreach (JObject item in lista)
            {
                return item;
            }
            return JObject.Parse("{}");
        }

        public bool Criar(JObject body)
        {
            bool retorno = false;
            try
            {
                IRestResponse response = md.Documents(body);
                if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    if (response.Content == "null")
                    {
                        return true;
                    }
                    else
                    {
                        if (body["Email"].Count() == 1)
                        {
                            Erros.Numero = 1;
                            Erros.Mensagem = "O cliente (" + body["Email"].ToString() + ") já existe na VTEX.";
                        }
                        else
                        {
                            Erros.Numero = Convert.ToInt32(response.StatusCode);
                            Erros.Mensagem = response.Content;
                        }
                    }
                }
                return retorno;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return false;
            }
        }
    }
}
