﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using VTEX.API.Raml;
using VTEX.Webservice.WSDL;
using VTEX.Webservice;

namespace XPBI.Integracao.VTex.Logistics
{
    public class Estoque: Configuracao
    {
        private VTEX.API.Raml.Logistics apiLogistica;

        public Estoque(string loja, string appKey, string appToken)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            apiLogistica = new VTEX.API.Raml.Logistics(Loja, AppKey, AppToken);
        }

        public Estoque(string loja, string appKey, string appToken, string userSoap, string passSoap)
        {
            Loja = loja;
            AppKey = appKey;
            AppToken = appToken;
            UserSoap = userSoap;
            PassSoap = passSoap;
            apiLogistica = new VTEX.API.Raml.Logistics(Loja, AppKey, AppToken);
        }

        /// <summary>
        /// importa para a VTEX todas as lojas da XP
        /// </summary>
        /// <returns></returns>
        public bool Criar(string id, string nome)
        {
            JArray wareHouseDocks = new JArray();
            JObject dock = new JObject();
            dock.Add("dockId", "1");
            dock.Add("name", "Centro de Distribuição Principal");
            dock.Add("time", "0.00:00:00");//3.00:00:00 = 3 dias
            dock.Add("cost", "0.00");
            dock.Add("translateDays", "dias");
            dock.Add("costToDisplay", "0.00");
            wareHouseDocks.Add(dock);
            return Criar(id, nome, wareHouseDocks);
        }

        public bool Criar(string id, string nome, JArray wareHouseDocks)
        {
            bool retorno = false;
            try
            {
                //verifica se existe. Se sim, ab
                IRestResponse response = apiLogistica.GetWarehouseById(id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    if (response.Content == "null")
                    {
                        JObject objeto = new JObject();
                        objeto.Add("id", id);
                        objeto.Add("name", nome);
                        objeto.Add("warehouseDocks", wareHouseDocks);

                        response = apiLogistica.CreateUpdateWarehouse(objeto.ToString());
                        if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.Created)
                        {
                            return true;
                        }
                        else
                        {
                            Erros.Numero = Convert.ToInt32(response.StatusCode);
                            Erros.Mensagem = response.Content;
                        }

                    }
                    else
                    {
                        Erros.Numero = 1;
                        Erros.Mensagem = "O estoque (" + id + ") já existe na VTEX.";
                    }
                }
                return retorno;
            }catch(Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return false;
            }
        }

        public JObject Get(string id)
        {
            JObject retorno = new JObject();
            try
            {
                IRestResponse response = apiLogistica.GetWarehouseById(id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    if (response.Content != "null")
                    {
                        retorno = JObject.Parse(response.Content);
                    }
                    else
                    {
                        Erros.Numero = 1;
                        Erros.Mensagem = "O estoque (" + id + ") não existe na VTEX.";
                        retorno = JObject.Parse("{}"); 
                    }
                }
                return retorno;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return JObject.Parse("{}");
            }
        }

        public JObject InventoryBySku(int id)
        {
            JObject retorno = new JObject();
            try
            {
                IRestResponse response = apiLogistica.InventoryBySku(id.ToString());
                if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    if (response.Content != "null")
                    {
                        retorno = JObject.Parse(response.Content);
                    }
                    else
                    {
                        Erros.Numero = 1;
                        Erros.Mensagem = "O sku (" + id + ") não existe na VTEX.";
                        retorno = JObject.Parse("{}");
                    }
                }
                return retorno;
            }
            catch (Exception ex)
            {
                Erros.Numero = ex.HResult;
                Erros.Mensagem = ex.Message;
                return JObject.Parse("{}");
            }
        }
    }
}
